/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.rpc;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaBusService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.QueryBuilderService;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.ibatis.extension.toolkit.Constants;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "metaBusService")
public class MetaBusServiceImpl implements MetaBusService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private QueryBuilderService queryBuilderService;

    @Override
    public void clearMyBatisCache() {
        metaService.clearMyBatisCache();
    }

    @Override
    public DynaBean insertByTableCode(String tableCode, DynaBean beanMap) {
        if (Strings.isNullOrEmpty(tableCode)) {
            tableCode = beanMap.getStr(Constants.KEY_TABLE_CODE);
        }
        metaService.insert(tableCode, beanMap);
        return beanMap;
    }

    @Override
    public List<DynaBean> insertBatchByTableCode(String tableCode, List<DynaBean> list) {
        metaService.insertBatch(tableCode, list);
        return list;
    }

    @Override
    public DynaBean update(String tableCode, String pkValue, DynaBean beanMap, NativeQuery nativeQuery) {
        metaService.update(tableCode, pkValue, beanMap, queryBuilderService.buildWrapper(nativeQuery));
        return null;
    }

    @Override
    public int deleteByTableCodeAndNativeQuery(String tableCode, NativeQuery nativeQuery) {
        return metaService.delete(tableCode, queryBuilderService.buildWrapper(nativeQuery));
    }

    @Override
    public List<Map<String, Object>> selectMapByPageAndNativeQuery(Page page, NativeQuery nativeQuery) {
        return metaService.selectSql(page, queryBuilderService.buildWrapper(nativeQuery));
    }

    @Override
    public List<DynaBean> selectPageWithColumns(String tableCode, Page page, NativeQuery nativeQuery, String columns) {
        return metaService.select(tableCode, page, queryBuilderService.buildWrapper(nativeQuery), columns);
    }

    @Override
    public DynaBean selectOne(String tableCode, NativeQuery nativeQuery, String columns) {
        return metaService.selectOne(tableCode, queryBuilderService.buildWrapper(nativeQuery), columns);
    }

    @Override
    public List<Map<String, Object>> loadByNativeQuery(String funcCode, Page page, NativeQuery nativeQuery) {
        return metaService.load(funcCode, page, queryBuilderService.buildWrapper(nativeQuery));
    }

    @Override
    public DynaBean selectOneByPkWithColumns(String tableCode, String pkValue, String columns) {
        return metaService.selectOneByPk(tableCode, pkValue, columns);
    }

    @Override
    public int executeSqlByNativeQuery(NativeQuery nativeQuery) {
        return metaService.executeSql(queryBuilderService.buildWrapper(nativeQuery));
    }

    @Override
    public long countByNativeQuery(NativeQuery nativeQuery) {
        return metaService.countBySql(queryBuilderService.buildWrapper(nativeQuery));
    }

    @Override
    public Boolean selectDataExistsByPkValue(String tableCode, String pkValue) {
        try {
            if (metaService.selectOneByPk(tableCode, pkValue, null) == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //覆写

    @Override
    public List<Map<String, Object>> selectMap(String sql, Object... params) {
        return MetaBusService.super.selectMap(sql, params);
    }

}
