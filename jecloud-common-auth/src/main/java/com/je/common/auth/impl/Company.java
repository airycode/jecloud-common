/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import com.je.common.auth.AuthModel;
import java.util.Map;
import static com.je.common.auth.util.BeanMapUtil.*;

/**
 * 公司
 */
public class Company extends TenantModel implements AuthModel {

    private static final long serialVersionUID = 9108854125483416212L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 管理员ID
     */
    private String managerId;
    /**
     * 管理员编码
     */
    private String managerCode;
    /**
     * 管理员密码
     */
    private String managerName;
    /**
     * 主管ID
     */
    private String majorId;
    /**
     * 主管编码
     */
    private String majorCode;
    /**
     * 主管名称
     */
    private String majorName;
    /**
     * 公司级别编码
     */
    private String levelCode;
    /**
     * 级别名称
     */
    private String levelName;
    /**
     * 办公地址
     */
    private String address;
    /**
     * 父级公司
     */
    private String parent;
    /**
     * 集团公司ID
     */
    private String groupCompanyId;
    /**
     * 集团公司名称
     */
    private String groupCompanyName;
    /**
     * 公司路径
     */
    private String path;
    /**
     * 节点类型
     */
    private String nodeType;
    /**
     * 层级
     */
    private int layer;
    /**
     * 是否启用
     */
    private boolean disabled;
    /**
     * 树形排序
     */
    private String treeOrderIndex;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerCode() {
        return managerCode;
    }

    public void setManagerCode(String managerCode) {
        this.managerCode = managerCode;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getMajorId() {
        return majorId;
    }

    public void setMajorId(String majorId) {
        this.majorId = majorId;
    }

    public String getMajorCode() {
        return majorCode;
    }

    public void setMajorCode(String majorCode) {
        this.majorCode = majorCode;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getLevelCode() {
        return levelCode;
    }

    public void setLevelCode(String levelCode) {
        this.levelCode = levelCode;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGroupCompanyId() {
        return groupCompanyId;
    }

    public void setGroupCompanyId(String groupCompanyId) {
        this.groupCompanyId = groupCompanyId;
    }

    public String getGroupCompanyName() {
        return groupCompanyName;
    }

    public void setGroupCompanyName(String groupCompanyName) {
        this.groupCompanyName = groupCompanyName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getTreeOrderIndex() {
        return treeOrderIndex;
    }

    public void setTreeOrderIndex(String treeOrderIndex) {
        this.treeOrderIndex = treeOrderIndex;
    }

    @Override
    public void parse(Map<String,Object> beanMap) {
        super.parse(beanMap);
        this.setId(getStr(beanMap,"JE_RBAC_COMPANY_ID"));
        this.setCode(getStr(beanMap,"COMPANY_CODE"));
        this.setName(getStr(beanMap,"COMPANY_NAME"));
        this.setManagerId(getStr(beanMap,"COMPANY_MANAGER_ID"));
        this.setManagerCode(getStr(beanMap,"COMPANY_MANAGER_CODE"));
        this.setManagerName(getStr(beanMap,"COMPANY_MANAGER_NAME"));
        this.setMajorId(getStr(beanMap,"COMPANY_MAJOR_ID"));
        this.setMajorCode(getStr(beanMap,"COMPANY_MAJOR_CODE"));
        this.setMajorName(getStr(beanMap,"COMPANY_MAJOR_NAME"));
        this.setLevelCode(getStr(beanMap,"COMPANY_LEVEL_CODE"));
        this.setLevelName(getStr(beanMap,"COMPANY_LEVEL_NAME"));
        this.setAddress(getStr(beanMap,"COMPANY_ADDRESS"));
        this.setPath(getStr(beanMap,"SY_PATH"));
        this.setNodeType(getStr(beanMap,"SY_NODETYPE"));
        this.setLayer(getInteger(beanMap,"SY_LAYER"));
        this.setDisabled("1".equals(getStr(beanMap,"SY_DISABLED")) ? true : false);
        this.setTreeOrderIndex(getStr(beanMap,"SY_TREEORDERINDEX"));
        this.setParent(getStr(beanMap,"SY_PARENT"));
        this.setGroupCompanyId(getStr(beanMap,"SY_GROUP_COMPANY_ID"));
        this.setGroupCompanyName(getStr(beanMap,"SY_GROUP_COMPANY_NAME"));
    }

}
