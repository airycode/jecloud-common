/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import static com.je.common.auth.util.BeanMapUtil.*;

/**
 * 公司部门
 */
public class Department extends RealOrganization implements Serializable {

    private static final long serialVersionUID = 5192096016667502052L;
    /**
     * 资源表编码
     */
    private String tableCode = "JE_RBAC_DEPARTMENT";
    /**
     * 资源表编码主键
     */
    private String tablePkCode = "JE_RBAC_DEPARTMENT_ID";

    /**
     * 所属公司ID
     */
    private String companyId;
    /**
     * 所属公司编码
     */
    private String companyName;
    /**
     * 所属集团公司ID
     */
    private String groupCompanyId;
    /**
     * 所属集团公司Name
     */
    private String groupCompanyName;
    /**
     * 主管ID
     */
    private String majorId;
    /**
     * 主管编码
     */
    private String majorCode;
    /**
     * 主管名称
     */
    private String majorName;
    /**
     * 职能描述
     */
    private String funcDesc;
    /**
     * 父级部门
     */
    private String parent;
    /**
     * 部门路径
     */
    private String path;
    /**
     * 节点类型
     */
    private String nodeType;
    /**
     * 层级
     */
    private int layer;
    /**
     * 是否启用
     */
    private boolean disabled;
    /**
     * 树形排序
     */
    private String treeOrderIndex;
    /**
     * 是否默认主部门
     */
    private boolean main;
    /**
     * 部门监管部门
     */
    private List<String> monitorDepts;

    @Override
    public String getTableCode() {
        return tableCode;
    }

    @Override
    public String getTablePkCode() {
        return tablePkCode;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupCompanyId() {
        return groupCompanyId;
    }

    public void setGroupCompanyId(String groupCompanyId) {
        this.groupCompanyId = groupCompanyId;
    }

    public String getGroupCompanyName() {
        return groupCompanyName;
    }

    public void setGroupCompanyName(String groupCompanyName) {
        this.groupCompanyName = groupCompanyName;
    }

    public String getMajorId() {
        return majorId;
    }

    public void setMajorId(String majorId) {
        this.majorId = majorId;
    }

    public String getMajorCode() {
        return majorCode;
    }

    public void setMajorCode(String majorCode) {
        this.majorCode = majorCode;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getFuncDesc() {
        return funcDesc;
    }

    public void setFuncDesc(String funcDesc) {
        this.funcDesc = funcDesc;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getTreeOrderIndex() {
        return treeOrderIndex;
    }

    public void setTreeOrderIndex(String treeOrderIndex) {
        this.treeOrderIndex = treeOrderIndex;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public List<String> getMonitorDepts() {
        return monitorDepts;
    }

    public void setMonitorDepts(List<String> monitorDepts) {
        this.monitorDepts = monitorDepts;
    }

    @Override
    public void parse(Map<String,Object> beanMap) {
        super.parse(beanMap);
        this.setId(getStr(beanMap,"JE_RBAC_DEPARTMENT_ID"));
        this.setCode(getStr(beanMap,"DEPARTMENT_CODE"));
        this.setName(getStr(beanMap,"DEPARTMENT_NAME"));
        this.setMajorId(getStr(beanMap,"DEPARTMENT_MAJOR_ID"));
        this.setMajorCode(getStr(beanMap,"DEPARTMENT_MAJOR_CODE"));
        this.setMajorName(getStr(beanMap,"DEPARTMENT_MAJOR_NAME"));
        this.setFuncDesc(getStr(beanMap,"DEPARTMENT_FUNC_DESC"));
        this.setPath(getStr(beanMap,"SY_PATH"));
        this.setNodeType(getStr(beanMap,"SY_NODETYPE"));
        this.setLayer(getInteger(beanMap,"SY_LAYER"));
        this.setDisabled("1".equals(getStr(beanMap,"SY_DISABLED")) ? true : false);
        this.setMain("1".equals(getStr(beanMap,"DEPTUSER_MAIN_CODE")) ? true : false);
        this.setTreeOrderIndex(getStr(beanMap,"SY_TREEORDERINDEX"));
        String monitDeptIds = getStr(beanMap,"DEPARTMENT_MONITORDEPT_ID");
        if (!Strings.isNullOrEmpty(monitDeptIds)) {
            this.setMonitorDepts(Splitter.on(",").splitToList(monitDeptIds));
        }
        this.setParent(getStr(beanMap,"SY_PARENT"));
        this.setCompanyId(getStr(beanMap,"SY_COMPANY_ID"));
        this.setCompanyName(getStr(beanMap,"SY_COMPANY_NAME"));
        this.setGroupCompanyId(getStr(beanMap,"SY_GROUP_COMPANY_ID"));
        this.setGroupCompanyName(getStr(beanMap,"SY_GROUP_COMPANY_NAME"));
    }

}
