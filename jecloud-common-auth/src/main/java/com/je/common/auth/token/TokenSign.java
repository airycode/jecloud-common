/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.token;

import java.io.Serializable;

/**
 * Token 签名 Model 
 * 
 * 挂在到SaSession上的token签名
 * 
 * @author kong
 *
 */
public class TokenSign implements Serializable {

	private static final long serialVersionUID = 1406115065849845073L;

	/**
	 * token值
	 */
	private String value;

	/**
	 * 所在设备标识
	 */
	private String device;

	/** 构建一个 */
	public TokenSign() {
	}

	/**
	 * 构建一个
	 * 
	 * @param value  token值
	 * @param device 所在设备标识
	 */
	public TokenSign(String value, String device) {
		this.value = value;
		this.device = device;
	}

	/**
	 * @return token值 
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return token登录设备 
	 */
	public String getDevice() {
		return device;
	}

	@Override
	public String toString() {
		return "TokenSign [value=" + value + ", device=" + device + "]";
	}

}
