/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl.account;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;
import com.je.common.auth.AuthAccount;
import com.je.common.auth.AuthOrg;
import com.je.common.auth.impl.PlatformOrganization;
import com.je.common.auth.impl.RealOrganizationUser;
import com.je.common.auth.impl.TenantModel;
import com.je.common.auth.impl.role.Role;

import java.util.*;

import static com.je.common.auth.util.BeanMapUtil.getStr;

/**
 * 账户，账户必须从属于机构，并且需要关联一个真实用户，此用户可以是任何机构管理绑定的用户
 *
 * @author liulijun
 */
public class Account extends TenantModel implements AuthAccount {

    private static final long serialVersionUID = -8927232433945532171L;

    /**
     * 主键ID
     */
    private String id;
    /**
     * 机构用户名称
     */
    private String name;
    /**
     * 机构用户编码
     */
    private String code;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 邮件
     */
    private String email;
    /**
     * 开放ID
     */
    private String openId;
    /**
     * 过期时间
     */
    private Date expireTime;
    /**
     * 是否锁定
     */
    private boolean locked;
    /**
     * 账户卡号
     */
    private String cardNum;
    /**
     * 所属机构
     */
    private PlatformOrganization platformOrganization;
    /**
     * 真实用户
     */
    private RealOrganizationUser realUser;
    /**
     * 角色集合
     */
    private List<Role> roles;
    /**
     * 权限集合
     */
    private Set<String> permissions;
    /**
     * 账号部门视图id
     */
    private String deptId;

    public Account() {
    }

    public Account(PlatformOrganization platformOrganization, RealOrganizationUser realUser, List<Role> roles, Set<String> permissions) {
        this.roles = roles;
        this.permissions = permissions;
        this.realUser = realUser;
        this.platformOrganization = platformOrganization;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public AuthOrg getAuthOrg() {
        return platformOrganization;
    }

    @Override
    public RealOrganizationUser getRealUser() {
        return realUser;
    }

    @Override
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public void setRealUser(RealOrganizationUser realUser) {
        this.realUser = realUser;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    public PlatformOrganization getPlatformOrganization() {
        return platformOrganization;
    }

    public void setPlatformOrganization(PlatformOrganization platformOrganization) {
        this.platformOrganization = platformOrganization;
    }

    public List<String> getRoleIds() {
        if (roles == null) {
            return null;
        }
        List<String> roleIdList = new ArrayList<>();
        for (Role eachRole : roles) {
            roleIdList.add(eachRole.getId());
        }
        return roleIdList;
    }

    public List<String> getRoleCodes() {
        if (roles == null) {
            return null;
        }
        List<String> roleCodeList = new ArrayList<>();
        for (Role eachRole : roles) {
            roleCodeList.add(eachRole.getCode());
        }
        return roleCodeList;
    }

    public List<String> getRoleNames() {
        if (roles == null) {
            return null;
        }
        List<String> roleNameList = new ArrayList<>();
        for (Role eachRole : roles) {
            roleNameList.add(eachRole.getName());
        }
        return roleNameList;
    }

    @Override
    public void parse(Map<String,Object> beanMap) {
        super.parse(beanMap);
        this.setId(getStr(beanMap,"JE_RBAC_ACCOUNT_ID"));
        this.setCode(getStr(beanMap,"ACCOUNT_CODE"));
        this.setName(getStr(beanMap,"ACCOUNT_NAME"));
        this.setPhone(getStr(beanMap,"ACCOUNT_PHONE"));
        this.setEmail(getStr(beanMap,"ACCOUNT_MAIL"));
        this.setAvatar(getStr(beanMap,"ACCOUNT_AVATAR"));
        this.setRemark(getStr(beanMap,"ACCOUNT_REMARK"));

        this.setOpenId(getStr(beanMap,"ACCOUNT_OPENID"));
        this.setExpireTime(Strings.isNullOrEmpty(getStr(beanMap,"ACCOUNT_EXPIRE_TIME")) ? null : DateUtil.parseDateTime(getStr(beanMap,"ACCOUNT_EXPIRE_TIME")));
        this.setLocked("1".equals(getStr(beanMap,"ACCOUNT_LOCKED_CODE")) ? true : false);
        this.setCardNum(getStr(beanMap,"ACCOUNT_CARDNUM"));
    }

}
