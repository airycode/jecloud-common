/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;
import com.je.common.auth.AuthModel;

import java.util.Date;
import java.util.Map;
import static com.je.common.auth.util.BeanMapUtil.getLong;
import static com.je.common.auth.util.BeanMapUtil.getStr;

/**
 * Rbac基础对象
 */
public abstract class RbacModel implements AuthModel {

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人ID
     */
    private String createUserId;
    /**
     * 创建人名称
     */
    private String createUserName;
    /**
     * 创建部门ID
     */
    private String createDeptId;
    /**
     * 创建部门名称
     */
    private String createDeptName;
    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;
    /**
     * 最后更新人ID
     */
    private String lastUpdateUserId;
    /**
     * 最后更新人名称
     */
    private String lastUpdateUserName;
    /**
     * 排序
     */
    private long orderIndex;
    /**
     * 状态
     */
    private String status;
    /**
     * 备注信息
     */
    private String remark;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(String createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public String getLastUpdateUserName() {
        return lastUpdateUserName;
    }

    public void setLastUpdateUserName(String lastUpdateUserName) {
        this.lastUpdateUserName = lastUpdateUserName;
    }

    public long getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(long orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void parse(Map<String, Object> beanMap) {
        //创建信息
        this.setCreateTime(Strings.isNullOrEmpty(getStr(beanMap,"SY_CREATETIME")) ? null : DateUtil.parseDateTime(getStr(beanMap,"SY_CREATETIME")));
        this.setCreateUserId(getStr(beanMap,"SY_CREATEUSERID"));
        this.setCreateUserName(getStr(beanMap,"SY_CREATEUSERNAME"));
        this.setCreateDeptId(getStr(beanMap,"SY_CREATEORGID"));
        this.setCreateDeptName(getStr(beanMap,"SY_CREATEORGNAME"));
        //更新信息
        this.setLastUpdateTime(Strings.isNullOrEmpty(getStr(beanMap,"SY_MODIFYTIME")) ? null : DateUtil.parseDateTime(getStr(beanMap,"SY_MODIFYTIME")));
        this.setLastUpdateUserId(getStr(beanMap,"SY_MODIFYUSERID"));
        this.setLastUpdateUserName(getStr(beanMap,"SY_MODIFYUSERNAME"));
        //排序信息
        this.setOrderIndex(getLong(beanMap,"SY_ORDERINDEX"));
        //状态信息
        this.setStatus(getStr(beanMap,"SY_STATUS"));
    }

}
