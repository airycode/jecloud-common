/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb.filter;

import com.je.common.base.context.GlobalExtendedContext;
import com.je.common.base.spring.SpringContextHolder;
import org.apache.servicecomb.common.rest.filter.HttpServerFilter;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

import static com.je.common.base.util.SystemSecretUtil.*;

/**
 * 密级设置上下文拦截器
 */
public class SecretSettingContextFilter extends AbstractHttpServerFilter implements HttpServerFilter {

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {
        RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");
        Map result = redisTemplate.opsForHash().entries("secretSettingCache");
        if (result == null || result.isEmpty()
                || !result.containsKey(SECRET_SWITCH_KEY)
                || !"1".equals(result.get(SECRET_SWITCH_KEY))) {
            GlobalExtendedContext.removeContext(SECRET_SWITCH_KEY, SECRET_CODE_KEY, SECRET_CODE_NAME);
            return null;
        }
        result.forEach((key, value) -> {
            GlobalExtendedContext.put(key.toString(), value == null ? null : value.toString());
        });
        return null;
    }

}
