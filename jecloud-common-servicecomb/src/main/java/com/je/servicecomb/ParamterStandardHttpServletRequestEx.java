/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb;

import org.apache.servicecomb.foundation.vertx.http.StandardHttpServletRequestEx;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义可以set参数的request
 */
public class ParamterStandardHttpServletRequestEx extends StandardHttpServletRequestEx {

    private Map<String , String[]> params = new HashMap<String, String[]>();

    public ParamterStandardHttpServletRequestEx(HttpServletRequest request) {
        super(request);
    }

    public ParamterStandardHttpServletRequestEx(HttpServletRequest request,Map<String , String[]> extendParams) {
        this(request);
        params.putAll(extendParams);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(this.getParameterMap().keySet());
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        Map<String,String[]> tempResult = super.getParameterMap();
        this.params.putAll(tempResult);
        return this.params;
    }

    @Override
    public String getParameter(String name) {
        String[] values = (String[])this.getParameterMap().get(name);
        return values == null ? null : values[0];
    }

    @Override
    public void setParameter(String name, String value) {
        this.params.put(name,new String[]{value});
    }

    @Override
    public String[] getParameterValues(String name) {
        return (String[])this.getParameterMap().get(name);
    }

}
