/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb.exception;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.swagger.invocation.Response;
import org.apache.servicecomb.swagger.invocation.SwaggerInvocation;
import org.apache.servicecomb.swagger.invocation.exception.ExceptionToProducerResponseConverter;
import org.apache.servicecomb.swagger.invocation.exception.InvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @program: jecloud-workflow
 * @author: LIULJ
 * @create: 2021/7/9
 * @description:
 */

public class
CloudPlatformExceptionToProducerResponseConverter implements ExceptionToProducerResponseConverter<PlatformException> {

    private static final Logger logger = LoggerFactory.getLogger(CloudPlatformExceptionToProducerResponseConverter.class);

    /**
     * 异常处理顺序
     */
    private static final int ORDER = 1;

    @Override
    public Class<PlatformException> getExceptionClass() {
        return PlatformException.class;
    }

    @Override
    public Response convert(SwaggerInvocation swaggerInvocation, PlatformException e) {
        logger.error(ExceptionUtil.getMessage(e));
        String url = ((Invocation)swaggerInvocation).getRequestEx().getRequestURI();
        logger.info("throw PlatformException error...........url....." + url);
        logger.info("throw PlatformException error...........context....." + ((Invocation) swaggerInvocation).getContext().toString());
        BaseRespResult baseRespResult = BaseRespResult.errorResult(e.getErrorCode(), e.getErrorParam(), e.getErrorMsg());
        InvocationException state = new InvocationException(javax.ws.rs.core.Response.Status.OK, baseRespResult);
        logger.error("log.isErrorEnabled():{}", logger.isErrorEnabled());
        logger.info("log.isInfoEnabled():{}", logger.isInfoEnabled());
        e.printStackTrace();
        CloudExceptionInfoStorage.saveExceptionLog(url,((Invocation) swaggerInvocation).getRequestEx().getParameterMap(),baseRespResult.getMessage(),baseRespResult.getCode(),e.getStackTrace());
        return Response.succResp(baseRespResult);
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}
