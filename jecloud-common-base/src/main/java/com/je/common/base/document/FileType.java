/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.document;

import java.io.Serializable;

/**
 * 文件类型VO
 * @author zhangshuaipeng
 *
 */
public class FileType implements Serializable{

	private static final long serialVersionUID = -4432998823352761456L;
	private String name;//名称
	private String code;//编码
	private String iconCls;//图片
//	private String thumbnailInfo;//缩略图信息
//	private String bigIconCls;//TODO未处理
//	private String thumbnailCls;//TODO未处理
//	private String format;//TODO未处理
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
//	public String getBigIconCls() {
//		return bigIconCls;
//	}
//	public void setBigIconCls(String bigIconCls) {
//		this.bigIconCls = bigIconCls;
//	}
//	public String getThumbnailCls() {
//		return thumbnailCls;
//	}
//	public void setThumbnailCls(String thumbnailCls) {
//		this.thumbnailCls = thumbnailCls;
//	}
//	public String getFormat() {
//		return format;
//	}
//	public void setFormat(String format) {
//		this.format = format;
//	}
//
//	public String getThumbnailInfo() {
//		return thumbnailInfo;
//	}
//
//	public void setThumbnailInfo(String thumbnailInfo) {
//		this.thumbnailInfo = thumbnailInfo;
//	}
}
