/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.portal.service;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.portal.vo.Bubble;
import com.je.common.base.portal.vo.Comment;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BubblePortalService {

    /**
     * 保存冒泡信息到数据库
     * @param bubble TODO 暂不明确
     * @return
     */
    DynaBean saveDataBaseBubble(Bubble bubble);

    /**
     * 数据库查询冒泡
     * @param type 最热或者最新
     * @param start 起始数据
     * @param limit 每页多少条数据
     * @return
     */
    HashMap findDataBaseBubble(String type, int start, int limit);

    /**
     * 获取冒泡数字
     * @param userId 用户id
     * @return
     */
    JSONObject getBubbleNum(String userId);


    /**
     * 获取冒泡数字
     * @param userId 用户id
     * @return
     */
    JSONObject getBubbleBadge(String userId);

    /**
     * 更新点赞数评论数
     */
    void updateBubbleNum(String bubbleId, String type, String oper);

    /**
     * 点赞
     */
    void thumbsUp(String source, String id, DynaBean mpxxBean);

    /**
     * 取消点赞
     */
    void cancelThumbsUp(String source, String id);

    List<Map> isThumbsUp(List<Map> list);

    /**
     * 保存评论
     */
    DynaBean saveComment(Comment comment);

    List<HashMap> findComment(Comment comment);

    List thumbsUpAndComment(List<Map> list);

    /**
     * 保存消息
     * @param type 消息类型
     * @param dynaBean
     */
    void sendMsg(String type, String source, DynaBean dynaBean, String sourceMpNr);
}
