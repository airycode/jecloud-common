/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.upgrade;

import com.je.common.base.DynaBean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PackageResult implements Serializable {

    /**
     * 升级BEAN
     */
    private DynaBean upgrade;
    /**
     * 产品Bean
     */
    private DynaBean product;
    /**
     * 产品功能
     */
    private List<DynaBean> productFunctions;
    /**
     * 产品资源表
     */
    private List<DynaBean> productTables;
    /**
     * 数据字典
     */
    private List<DynaBean> productDictionarys;
    /**
     * 菜单
     */
    private List<DynaBean> productMenus;
    /**
     * 顶部菜单
     */
    private List<DynaBean> productTopMenus;
    /**
     * 工作流
     */
    private List<DynaBean> productWorkflows;
    /**
     * 系统变量
     */
    private List<DynaBean> productSystemVariables;
    /**
     * 系统设置
     */
    private List<DynaBean> productSystemSettings;
    /**
     * 全局变量
     */
    private List<DynaBean> globalScripts;
    /**
     * 产品角色
     */
    private List<DynaBean> productRoles;
    /**
     * 产品门户
     */
    private List<DynaBean> productPartols;
    /**
     * 产品报表
     */
    private List<DynaBean> productReports;
    /**
     * 产品图表
     */
    private List<DynaBean> productCharts;
    /**
     * 产品数据源
     */
    private List<DynaBean> productDataSource;
    /**
     * 业务数据
     */
    private Map<String,List<DynaBean>> businessDataMap;
    /**
     * 文件key
     */
    private List<String> fileKeys;

    /**
     * 安装包主键值
     */
    private String installPackageId;

    public String getInstallPackageId() {
        return installPackageId;
    }

    public void setInstallPackageId(String installPackageId) {
        this.installPackageId = installPackageId;
    }

    public DynaBean getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(DynaBean upgrade) {
        this.upgrade = upgrade;
    }

    public DynaBean getProduct() {
        return product;
    }

    public void setProduct(DynaBean product) {
        this.product = product;
    }

    public List<DynaBean> getProductFunctions() {
        return productFunctions;
    }

    public void setProductFunctions(List<DynaBean> productFunctions) {
        this.productFunctions = productFunctions;
    }

    public List<DynaBean> getProductTables() {
        return productTables;
    }

    public void setProductTables(List<DynaBean> productTables) {
        this.productTables = productTables;
    }

    public List<DynaBean> getProductDictionarys() {
        return productDictionarys;
    }

    public void setProductDictionarys(List<DynaBean> productDictionarys) {
        this.productDictionarys = productDictionarys;
    }

    public List<DynaBean> getGlobalScripts() {
        return globalScripts;
    }

    public void setGlobalScripts(List<DynaBean> globalScripts) {
        this.globalScripts = globalScripts;
    }

    public List<DynaBean> getProductMenus() {
        return productMenus;
    }

    public void setProductMenus(List<DynaBean> productMenus) {
        this.productMenus = productMenus;
    }

    public List<DynaBean> getProductTopMenus() {
        return productTopMenus;
    }

    public void setProductTopMenus(List<DynaBean> productTopMenus) {
        this.productTopMenus = productTopMenus;
    }

    public List<DynaBean> getProductWorkflows() {
        return productWorkflows;
    }

    public void setProductWorkflows(List<DynaBean> productWorkflows) {
        this.productWorkflows = productWorkflows;
    }

    public List<DynaBean> getProductSystemVariables() {
        return productSystemVariables;
    }

    public void setProductSystemVariables(List<DynaBean> productSystemVariables) {
        this.productSystemVariables = productSystemVariables;
    }

    public List<DynaBean> getProductSystemSettings() {
        return productSystemSettings;
    }

    public void setProductSystemSettings(List<DynaBean> productSystemSettings) {
        this.productSystemSettings = productSystemSettings;
    }

    public List<DynaBean> getProductRoles() {
        return productRoles;
    }

    public void setProductRoles(List<DynaBean> productRoles) {
        this.productRoles = productRoles;
    }

    public List<DynaBean> getProductPartols() {
        return productPartols;
    }

    public void setProductPartols(List<DynaBean> productPartols) {
        this.productPartols = productPartols;
    }

    public List<DynaBean> getProductReports() {
        return productReports;
    }

    public void setProductReports(List<DynaBean> productReports) {
        this.productReports = productReports;
    }

    public List<DynaBean> getProductCharts() {
        return productCharts;
    }

    public void setProductCharts(List<DynaBean> productCharts) {
        this.productCharts = productCharts;
    }

    public List<DynaBean> getProductDataSource() {
        return productDataSource;
    }

    public void setProductDataSource(List<DynaBean> productDataSource) {
        this.productDataSource = productDataSource;
    }

    public Map<String, List<DynaBean>> getBusinessDataMap() {
        return businessDataMap;
    }

    public void setBusinessDataMap(Map<String, List<DynaBean>> businessDataMap) {
        this.businessDataMap = businessDataMap;
    }

    public List<String> getFileKeys() {
        return fileKeys;
    }

    public void setFileKeys(List<String> fileKeys) {
        this.fileKeys = fileKeys;
    }

}
