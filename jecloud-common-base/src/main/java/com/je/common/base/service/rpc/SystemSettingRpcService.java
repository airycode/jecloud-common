/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.rpc;

import com.je.common.base.document.DocumentPreviewWayEnum;

import java.util.Map;

public interface SystemSettingRpcService {

    /**
     * 根据Code获取系统设置
     * @param code
     * @return
     */
    String findSettingValue(String code);

    /**
     * 获取所有设置
     * @return
     */
    Map<String,String> requireAllSettingValue();

    /**
     * 写入系统设置
     * @param attribute
     * @param values
     */
    void writeValues(String attribute, Map<String,String> values);

    /**
     * 写入单个系统设置
     * @param attribute
     * @param key
     * @param value
     */
    void writeValue(String attribute,String key,String value);

    /**
     * 重载系统设置
     */
    void reloadSetting();

    /**
     * 是否是SaaS
     * @return
     */
    boolean isSaas();

    /**
     * 获取方案产品
     * @param plan
     * @return
     */
    String requirePlanProduct(String plan);

    /**
     * 验证是否是文档管理员
     * @param userId
     * @return
     */
    boolean checkDocumentAdmin(String userId);

    /**
     * Office文档预览方式
     */
    DocumentPreviewWayEnum getDocumentPreviewWayByFileSuffix(String fileSuffix);

}
