/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.entity.api;

public class FuncAssociationEntity {
    private String parentFieldName;
    private String parentFieldCode;
    private String childFieldName;
    private String childFieldCode;
    private boolean valueTransfer;
    private boolean whereSql;
    private String association;
    private String customWhereSql;

    public String getParentFieldName() {
        return parentFieldName;
    }

    public void setParentFieldName(String parentFieldName) {
        this.parentFieldName = parentFieldName;
    }

    public String getParentFieldCode() {
        return parentFieldCode;
    }

    public void setParentFieldCode(String parentFieldCode) {
        this.parentFieldCode = parentFieldCode;
    }

    public String getChildFieldName() {
        return childFieldName;
    }

    public void setChildFieldName(String childFieldName) {
        this.childFieldName = childFieldName;
    }

    public String getChildFieldCode() {
        return childFieldCode;
    }

    public void setChildFieldCode(String childFieldCode) {
        this.childFieldCode = childFieldCode;
    }

    public boolean isValueTransfer() {
        return valueTransfer;
    }

    public void setValueTransfer(boolean valueTransfer) {
        this.valueTransfer = valueTransfer;
    }

    public boolean isWhereSql() {
        return whereSql;
    }

    public void setWhereSql(boolean whereSql) {
        this.whereSql = whereSql;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getCustomWhereSql() {
        return customWhereSql;
    }

    public void setCustomWhereSql(String customWhereSql) {
        this.customWhereSql = customWhereSql;
    }
}
