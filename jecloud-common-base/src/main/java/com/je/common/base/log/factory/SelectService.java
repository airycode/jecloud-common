/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log.factory;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.aspectj.lang.ProceedingJoinPoint;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;


public interface SelectService {

    public static final String sql_text="sql_text";
    public static final String sql_parameter="sql_parameter";

    String getSql(ProceedingJoinPoint joinPoint);


    default String getMethodType(Method method, HashMap<String,String> map) {
        Parameter[] parameters = method.getParameters();
        String name ="";
        if(parameters!=null&&parameters.length>0){
            for(int i =0;i<parameters.length;i++){
                Parameter parameter = parameters[i];
                if(i==parameters.length-1){
                    name=name+parameter.getName();
                }else {
                    name=name+parameter.getName()+"@";
                }
            }
        }
        if(StringUtil.isNotEmpty(name)){
            return map.get(name);
        }
        return null;
    }

    default String applySqlByPage(Page page, String text) {
        int current =page.getCurrent();
        int size =page.getSize();
        if(current==-1 || size==-1){
            return text;
        }

        long startLine = current==0?0:(current-1)*size;

        return text+ " LIMIT "+startLine+","+size;
    }


    default JSONObject getSqlInfoByWrapper(ConditionsWrapper conditionsWrapper){
        JSONObject jsonObject = new JSONObject();
        String tableCode= conditionsWrapper.getTable();
        String selectColumns = conditionsWrapper.getSelectColumns();
        String sql = conditionsWrapper.getParameterSql();
        String text;
        if(StringUtil.isEmpty(selectColumns)){
            text = "SELECT * FROM " +tableCode;
        }else{
            text = "SELECT "+selectColumns+" FROM " +tableCode;
        }

        if(StringUtil.isNotEmpty(sql)){
            text =  text + " WHERE "+sql;
        }
        jsonObject.put(sql_text,text);
        jsonObject.put(sql_parameter, "");

        return jsonObject;
    }

}
