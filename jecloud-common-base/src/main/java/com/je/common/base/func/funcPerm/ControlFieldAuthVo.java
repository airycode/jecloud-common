/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm;

import java.io.Serializable;

/**
 * 控制字段授权
 */
public class ControlFieldAuthVo implements Serializable {
    /**
     * 登记人员id字段编码
     */
    private String userIdsFieldCode;
    /**
     * 登记部门ID字段编码
     */
    private String deptIdsFieldCode;
    /**
     * 登记公司ID字段编码
     */
    private String companyIdFieldCode;
    /**
     * 登记集团公司ID字段编码
     */
    private String groupCompanyIdFieldCode;

    public String getUserIdsFieldCode() {
        return userIdsFieldCode;
    }

    public void setUserIdsFieldCode(String userIdsFieldCode) {
        this.userIdsFieldCode = userIdsFieldCode;
    }

    public String getDeptIdsFieldCode() {
        return deptIdsFieldCode;
    }

    public void setDeptIdsFieldCode(String deptIdsFieldCode) {
        this.deptIdsFieldCode = deptIdsFieldCode;
    }

    public String getCompanyIdFieldCode() {
        return companyIdFieldCode;
    }

    public void setCompanyIdFieldCode(String companyIdFieldCode) {
        this.companyIdFieldCode = companyIdFieldCode;
    }

    public String getGroupCompanyIdFieldCode() {
        return groupCompanyIdFieldCode;
    }

    public void setGroupCompanyIdFieldCode(String groupCompanyIdFieldCode) {
        this.groupCompanyIdFieldCode = groupCompanyIdFieldCode;
    }
}
