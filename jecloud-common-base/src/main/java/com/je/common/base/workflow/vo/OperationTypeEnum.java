/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.workflow.vo;

public enum OperationTypeEnum {
    PROCESSED("已处理", "已处理"),
    SUBMIT("提交", "已审批"),
    NONE("未处理", "未处理"),
    PROCESSING("正在处理", "正在处理"),
    INVALID("作废", "审批作废"),
    DELEGATE("委托", "任务委托"),
    TRANSFER("转办", "任务转办"),
    CANCEL_DELEGATE("取消委托", "被撤销委托"),
    DISMISS("驳回", "审批驳回"),
    GO_BACK("退回", "审批退回"),
    RETRIEVE("取回", "任务被取回"),
    URGE("催办", "催办"),
    PASS("通过", "通过"),
    VETO("否决", "否决"),
    ABSTAIN("弃权", "弃权");

    private String name;
    /**
     * 展示在历史中的名称
     */
    private String showName;

    OperationTypeEnum(String name, String showName) {
        this.name = name;
        this.showName = showName;
    }

    public String getName() {
        return name;
    }

    public String getShowName() {
        return showName;
    }

    public static OperationTypeEnum getTypeByName(String name) {
        if (SUBMIT.getShowName().equals(name)) {
            return SUBMIT;
        }
        if (NONE.getShowName().equals(name)) {
            return NONE;
        }
        if (PROCESSING.getShowName().equals(name)) {
            return PROCESSING;
        }
        if (INVALID.getShowName().equals(name)) {
            return INVALID;
        }
        if (DELEGATE.getShowName().equals(name)) {
            return DELEGATE;
        }
        if (CANCEL_DELEGATE.getShowName().equals(name)) {
            return CANCEL_DELEGATE;
        }
        if (DISMISS.getShowName().equals(name)) {
            return DISMISS;
        }
        if (GO_BACK.getShowName().equals(name)) {
            return GO_BACK;
        }
        if (RETRIEVE.getShowName().equals(name)) {
            return RETRIEVE;
        }
        if (URGE.getShowName().equals(name)) {
            return URGE;
        }
        if (PASS.getShowName().equals(name)) {
            return PASS;
        }
        if (VETO.getShowName().equals(name)) {
            return VETO;
        }
        if (ABSTAIN.getShowName().equals(name)) {
            return ABSTAIN;
        }
        if (TRANSFER.getShowName().equals(name)) {
            return TRANSFER;
        }
        if (PROCESSED.getShowName().equals(name)) {
            return PROCESSED;
        }
        return null;
    }
}
