/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log;

import com.je.common.base.log.factory.FactorySelect;
import com.je.common.base.log.factory.SelectService;
import com.je.common.base.service.MateLogManageService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/*@Aspect
@Component*/
public class CheckSlowSqlAspect {

    private final Logger logger = LoggerFactory.getLogger(CheckSlowSqlAspect.class);

    private static final String sql_text="sql_text";
    private static final String sql_parameter="sql_parameter";

    @Autowired
    private MateLogManageService mateLogManageService;

    /**
     * 定义AOP签名 (切入所有使用鉴权注解的方法)
     */
    public static final String POINTCUT_SIGN = "@annotation(com.je.common.base.log.CheckSlowSql)";

    /**
     * 声明AOP签名
     */
    @Pointcut(POINTCUT_SIGN)
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Date beforTime = new Date();
        Object obj = joinPoint.proceed();
        Date afterTime = new Date();
        try{
            String sqlInfo =getSql(joinPoint,signature.getName());
            mateLogManageService.saveSlowSql(beforTime,afterTime,signature.getName(),sqlInfo);
        }catch (Exception e){
            logger.info("保存慢SQL日志失败");
        }
        return obj;
    }

    private String getSql(ProceedingJoinPoint joinPoint,String methodName) {
        SelectService selectService =  FactorySelect.getInstance(methodName);
        if(selectService==null){
            return null;
        }
        return selectService.getSql(joinPoint);

    }
}
