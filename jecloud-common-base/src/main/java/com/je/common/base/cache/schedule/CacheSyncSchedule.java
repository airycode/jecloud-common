/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache.schedule;

import com.google.common.base.Strings;
import com.je.common.base.cache.AbstractHashCache;
import com.je.common.base.cache.AbstractListCache;
import com.je.common.base.cache.Cache;
import com.je.common.base.cache.model.CacheSync;
import com.je.common.base.redis.service.RedisCache;
import com.je.common.base.spring.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 缓存同步线程
 */
@Component
public class  CacheSyncSchedule {

    private static final Logger logger = LoggerFactory.getLogger("[Cache sync timer]");

    private static final List<String> SYNCED_KEYS = new ArrayList<>();

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RedisCache redisCache;

    @Scheduled(cron = "0/30 * * * * ?")
    public void sync() {
        Set<String> keys = redisCache.getKeys(Cache.SYNC_KEY_PREFIX + "*");
        if (keys == null || keys.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.info("check empty keys to sysnc!");
            }
            synchronized (this) {
                SYNCED_KEYS.clear();
            }
            return;
        }

        logger.info("checked keys ", keys);
        List<String> requireSyncKeys = new ArrayList<>();
        for (String eachKey : SYNCED_KEYS) {
            if (!SYNCED_KEYS.contains(eachKey)) {
                requireSyncKeys.add(eachKey);
            }
        }

        if (requireSyncKeys.isEmpty()) {
            return;
        }

        List<Object> cacheSyncs = redisTemplate.opsForValue().multiGet(requireSyncKeys);
        logger.info("exec sysnc");
        for (Object each : cacheSyncs) {
            CacheSync eachCacheSync = (CacheSync) each;
            Object object = SpringContextHolder.getBean(eachCacheSync.getBeanName());
            if (object instanceof AbstractHashCache) {
                AbstractHashCache hashCache = (AbstractHashCache) object;
                if (Strings.isNullOrEmpty(eachCacheSync.getTenantId())) {
                    if (eachCacheSync.getKey() == null || eachCacheSync.getKey().isEmpty()) {
                        hashCache.localClear();
                    } else {
                        hashCache.localRemove(eachCacheSync.getKey());
                    }
                } else {
                    if (eachCacheSync.getKey() == null || eachCacheSync.getKey().isEmpty()) {
                        hashCache.localClear(eachCacheSync.getTenantId());
                    } else {
                        hashCache.localRemove(eachCacheSync.getTenantId(), eachCacheSync.getKey());
                    }
                }
            } else if (object instanceof AbstractListCache) {
                AbstractListCache listCache = (AbstractListCache) object;
                if (Strings.isNullOrEmpty(eachCacheSync.getTenantId())) {
                    listCache.localClear();
                } else {
                    listCache.localClear(eachCacheSync.getTenantId());
                }
            }
        }
        SYNCED_KEYS.addAll(keys);
        logger.info("exec sync succeed!");
    }

}
