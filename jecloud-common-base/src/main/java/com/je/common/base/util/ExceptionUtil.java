/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.alibaba.fastjson2.JSON;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 异常工具类
 */
public class ExceptionUtil {
    /**
     * 构建请求参数信息
     *
     * @return
     */
    public static Map<String, Object> buildRequestParam(HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        Enumeration<String> enumerations = request.getParameterNames();
        while (enumerations.hasMoreElements()) {
            String key = enumerations.nextElement();
            params.put(key, request.getParameter(key));
        }
//        EndUser currentUser=SecurityUserHolder.getCurrentUser();
//        params.put("操作用户：",currentUser.getUsername());
//        params.put("操作用户ID：",currentUser.getUserId());
//        params.put("操作数据源：",currentUser.getZhDs());
//        params.put("TOKENID：",SecurityUserHolder.getToken());
        return params;
    }

    public static String buildRequestParamToJson(HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        Enumeration<String> enumerations = request.getParameterNames();

        while (enumerations.hasMoreElements()) {
            String key = enumerations.nextElement();
            params.put(key, request.getParameter(key));
        }

//        EndUser currentUser=SecurityUserHolder.getCurrentUser();
//        params.put("操作用户：",currentUser.getUsername());
//        params.put("操作用户ID：",currentUser.getUserId());
//        params.put("操作数据源：",currentUser.getZhDs());
//        params.put("TOKENID：",SecurityUserHolder.getToken());

        return JSON.toJSONString(params);
    }
}
