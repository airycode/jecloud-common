/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service.impl;

import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleSchemaStatVisitor;
import com.je.common.base.table.service.SqlParseService;
import com.je.common.base.util.SQLFormatterUtil;
import org.springframework.stereotype.Service;

import java.sql.SQLSyntaxErrorException;
@Service("dmSqlParseService")
public class DMSqlParseServiceImpl  extends AbstractSqlParseServiceImpl implements SqlParseService {
    private static final String dbType = "oracle";

    @Override
    public SQLSelectStatement selectParse(String selectSql) throws SQLSyntaxErrorException {
        return super.selectParse(selectSql, dbType);
    }

    @Override
    public String beautifySelect(String sql) throws SQLSyntaxErrorException {
        SQLSelectStatement statement = selectParse(sql, dbType);
        OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLAlterStatement alertParse(String selectSql) throws SQLSyntaxErrorException {
        return super.alertParse(selectSql, dbType);
    }

    @Override
    public SQLInsertStatement insertParse(String sql) throws SQLSyntaxErrorException {
        return super.insertParse(sql, dbType);
    }

    @Override
    public String beautifyInsert(String sql) throws SQLSyntaxErrorException {
        SQLInsertStatement statement = insertParse(sql, dbType);
        OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLUpdateStatement updateParse(String sql) throws SQLSyntaxErrorException {
        return super.updateParse(sql, dbType);
    }

    @Override
    public String beautifyUpdate(String sql) throws SQLSyntaxErrorException {
        SQLUpdateStatement statement = updateParse(sql, dbType);
        OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLCreateTableStatement createTableParse(String createTableDdl) throws SQLSyntaxErrorException {
        return super.createTableParse(createTableDdl, dbType);
    }

    @Override
    public String beautifyCreateTableDdl(String ddl) throws SQLSyntaxErrorException {
        SQLCreateTableStatement statement = createTableParse(ddl, dbType);
        OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public String beautifyAlertTableDdl(String ddl) throws SQLSyntaxErrorException {
        SQLAlterStatement statement = alertParse(ddl, dbType);
        OracleSchemaStatVisitor visitor = new OracleSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLCreateViewStatement createViewParse(String createViewDdl) throws SQLSyntaxErrorException {
        return super.createViewParse(createViewDdl, dbType);
    }

    @Override
    public String beautifyCreateViewDdl(String ddl) throws SQLSyntaxErrorException {
//        SQLCreateViewStatement statement = createViewParse(ddl, dbType);
//        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
//        statement.accept(visitor);
        return SQLFormatterUtil.format(ddl).trim();
    }

    public static void main(String[] args) throws SQLSyntaxErrorException {
        SqlParseService sqlParseService = new DMSqlParseServiceImpl();
//        String sql = sqlParseService.beautifySelect("select * from je_core_enduser where userid='1' and usercode='admin'");
//        System.out.println(sql);
        String ddl ="-- APP管理(JE_META_APPS) 表结构的创建语句 \n"+
                " CREATE TABLE DM_ONE (\n" +
                "\"SY_AUDFLAG\" VARCHAR2(20 char) null,\n" +
                "\"SY_COMPANY_ID\" VARCHAR2(50 char) null,\n" +
                "\"SY_COMPANY_NAME\" VARCHAR2(255 char) null,\n" +
                "\"SY_CREATEORGID\" VARCHAR2(50 char) null,\n" +
                "\"SY_CREATEORGNAME\" VARCHAR2(50 char) null,\n" +
                "\"SY_CREATETIME\" VARCHAR2(20 char) null,\n" +
                "\"SY_CREATEUSERID\" VARCHAR2(50 char) null,\n" +
                "\"SY_CREATEUSERNAME\" VARCHAR2(50 char) null,\n" +
                "\"SY_GROUP_COMPANY_ID\" VARCHAR2(50 char) null,\n" +
                "\"SY_GROUP_COMPANY_NAME\" VARCHAR2(255 char) null,\n" +
                "\"SY_ORDERINDEX\" NUMBER(20) null,\n" +
                "\"SY_ORG_ID\" VARCHAR2(50 char) null,\n" +
                "\"SY_PDID\" VARCHAR2(255 char) null,\n" +
                "\"SY_PIID\" VARCHAR2(255 char) null,\n" +
                "\"SY_STATUS\" VARCHAR2(4 char) null,\n" +
                "\"SY_TENANT_ID\" VARCHAR2(50 char) null,\n" +
                "\"SY_TENANT_NAME\" VARCHAR2(255 char) null,\n" +
                "\"TWO_NLLLL\" VARCHAR2(50 char) not null\n" +
                ");\n";
        String beautifyDdl = sqlParseService.beautifyCreateTableDdl(ddl);
        System.out.println(beautifyDdl);
    }
}
