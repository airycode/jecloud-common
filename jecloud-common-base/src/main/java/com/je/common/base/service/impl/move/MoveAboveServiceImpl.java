/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.impl.move;

import com.je.common.base.DynaBean;
import org.springframework.stereotype.Service;

/**
 * 在上面
 */

@Service("moveAboveService")
public class MoveAboveServiceImpl extends AbstractMoveService {

    @Override
    public void move(String tableCode, String id, String toId, DynaBean fromBean, DynaBean toResource) {
        //平级
        if (sameLevel(fromBean, toResource)) {
            levelExchange(tableCode, fromBean, toResource);
        } else {
            //不平级
            crossLevelExchange(tableCode, fromBean, toResource);
        }
    }

    /**
     * 跨级资源替换fromBean资源以及下面的子资源
     */
    public void crossLevelExchange(String tableCode, DynaBean resourceOne, DynaBean resourceTwo) {
        getUpdateSql(tableCode, resourceOne, resourceTwo, ABOVE);
    }


    public void levelExchange(String tableCode, DynaBean resourceOne, DynaBean resourceTwo) {
        getUpdateLevelAndChildSql(tableCode, resourceOne, resourceTwo, ABOVE);
    }

}
