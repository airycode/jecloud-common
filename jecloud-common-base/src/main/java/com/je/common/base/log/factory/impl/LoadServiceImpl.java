/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log.factory.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.log.factory.SelectService;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


@Service("loadService")
public class LoadServiceImpl implements SelectService {

    private static final HashMap<String,String> map = new HashMap();

    @Autowired
    private MetaResourceService metaResourceService;

    static {
        map.put("funcCode@page@sql@params","loadByPage4p");
        map.put("funcCode@page@wrapper","loadByPageAndWrapper3p");
    }
    
    
    @Override
    public String getSql(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodType = getMethodType(signature.getMethod(),map);

        JSONObject jsonObject = new JSONObject();
        
        switch (methodType){
            case "loadByPage4p":
                jsonObject =loadByPage4p(joinPoint.getArgs());
                break;
            case "loadByPageAndWrapper3p":
                jsonObject = loadByPageAndWrapper3p(joinPoint.getArgs());
                break;
        }
        return JSON.toJSONString(jsonObject);
    }

    private JSONObject loadByPageAndWrapper3p(Object[] args) {

        if(args==null||args.length!=3){
            return null;
        }

        if(args[2] instanceof ConditionsWrapper){
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[2];
            String funcCode = (String) args[0];
            String tableCode = getTableCodeByFuncCode(funcCode);
            conditionsWrapper.table(tableCode);
            JSONObject jsonObject = getSqlInfoByWrapper(conditionsWrapper);
            if(args[1] instanceof Page){
                Page page = (Page) args[1];
                String text = applySqlByPage(page,jsonObject.getString(sql_text));
                jsonObject.put(sql_text,text);
            }
            return jsonObject;
        }
        return null;
    }

    private JSONObject loadByPage4p(Object[] args) {
        if(args==null||args.length!=4){
            return null;
        }

        String sql = (String) args[2];
        Object[]  params = (Object[]) args[3];
        String funcCode = (String) args[0];
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        String tableCode = getTableCodeByFuncCode(funcCode);
        conditionsWrapper.table(tableCode);
        conditionsWrapper.apply(sql,params);
        JSONObject jsonObject = getSqlInfoByWrapper(conditionsWrapper);
        if(args[1] instanceof Page){
            Page page = (Page) args[1];
            String text = jsonObject.getString(sql_text);
            text = applySqlByPage(page,text);
            jsonObject.put(sql_text,text);
        }
        return jsonObject;

    }
    public String  getTableCodeByFuncCode(String funcCode){

        DynaBean valueBean = metaResourceService.selectOne("JE_CORE_FUNCINFO",
                NativeQuery.build().eq("FUNCINFO_FUNCCODE",funcCode),"FUNCINFO_TABLENAME");
        if(valueBean==null){
            return null;
        }
        return valueBean.getStr("FUNCINFO_TABLENAME");
    }
}
