/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.rpc;

import com.je.common.base.DynaBean;
import com.je.common.base.entity.func.FuncInfo;
import com.je.common.base.entity.func.FuncRelationField;
import java.util.List;
import java.util.Map;

public interface FunInfoRpcService {

    /**
     * 获取json的功能对象
     *
     * @param funcCode 功能编码
     * @return
     */
    FuncInfo getFuncInfo(String funcCode);

    /**
     * 获取功能配置对象
     *
     * @param funcInfo 功能信息
     * @param refresh  是否刷新，0：否，1：强制刷新
     * @return
     */
    Map<String, Object> getFuncConfigInfo(DynaBean funcInfo, boolean refresh);

    /**
     * 构建功能默认信息
     *
     * @param funcInfo TODO未处理
     */
    void buildDefaultFuncInfo(DynaBean funcInfo);

    /**
     * 更新功能信息(包括对按钮导入、软连接、资源表等做处理)
     *
     * @param funcInfo TODO未处理
     */
    DynaBean updateFunInfo(DynaBean funcInfo);

    /**
     * 删除功能   (包括对功能权限 按钮权限清除。 级联更新父节点信息 、删除挂有该功能的子功能信息和软连接关系清除)
     *
     * @param funcId TODO未处理
     */
    void removeFuncInfoById(String funcId);

    /**
     * 构建主子功能关联字段的查询条件
     *
     * @param relatedFields TODO未处理
     * @param dynaBean      TODO未处理
     */
    String buildWhereSql4funcRelation(List<FuncRelationField> relatedFields, DynaBean dynaBean);

    /**
     * 清空功能
     *
     * @param funcInfo TODO未处理
     */
    DynaBean clearFuncInfo(DynaBean funcInfo);

    /**
     * 初始化功能
     *
     * @param funcInfo TODO未处理
     */
    void initFuncInfo(DynaBean funcInfo);

    /**
     * 解除软连接关系
     *
     * @param funcRelyonId TODO未处理
     */
    void removeFuncRelyon(String funcRelyonId);

    /**
     * 功能字段配置同步
     *
     * @param funcId        当前功能主键
     * @param configFuncIds 配置信息参考功能主键
     */
    void syncConfig(String funcId, String configFuncIds);

}
