/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.extjs.Model;
import com.je.common.auth.impl.account.Account;
import java.util.List;

/**
 * 根据SQL查询数据
 */
public interface PcDbSQLService {

    /**
     * 查询数据˙˙
     * @param sql sql语句
     * @return
     */
    List<Model> selectModel(String sql);
    /**
     * 查询出SQL的名称
     * @param sql sql语句
     * @return
     */
    List<DynaBean> select(String sql);
    
    /**
     * 查询出SQL的名称
     * @param sql sql语句
     * @param limit 分页
     * @return
     */
    List<DynaBean> select(String sql, int limit);

    /**
     * 执行前台的事务操作
     * @param arrays
     * @param currentAccount 当前登陆人信息
     */
    void doTransaction(JSONArray arrays, Account currentAccount);

}
