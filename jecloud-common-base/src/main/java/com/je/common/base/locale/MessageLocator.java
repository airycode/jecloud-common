/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.locale;

import com.je.common.base.spring.SpringContextHolder;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import java.util.Locale;

public class MessageLocator {

    public static final String DEF_MSG = "后台处理出错了，请联系管理员！";

    private static MessageSource messageSource;

    static {
        messageSource = SpringContextHolder.getBean("messageSource");
        if (messageSource == null) {
            messageSource = new ResourceBundleMessageSource();
            ((ResourceBundleMessageSource) messageSource).setBasename("sys/locale/exception");
            ((ResourceBundleMessageSource) messageSource).setDefaultEncoding("UTF-8");
        }
    }


    public static String getMessage(String code) {
        return messageSource.getMessage(code, null, DEF_MSG, getLocal());
    }

    public static String getMessage(String code, String defMsg) {
        return messageSource.getMessage(code, null, defMsg, getLocal());
    }

    public static String getMessage(String code, Object[] args) throws NoSuchMessageException {
        return messageSource.getMessage(code, args, getLocal());
    }

    public static String getMessage(String code, Object[] args, String defMsg) {
        return messageSource.getMessage(code, args, defMsg, getLocal());
    }

    public static String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        return messageSource.getMessage(resolvable, locale);
    }

    //解析用户当前的Local
    public static Locale getLocal() {
        return LocaleContextHolder.getLocale();
    }
}
