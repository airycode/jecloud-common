/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.datasource;

import com.je.common.base.datasource.callable.IResultSetCallable;
import com.je.common.base.DynaBean;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
* Copyright: Copyright (c) 2018 jeplus.cn
* @Description: 动态Bean回调处理类
* @version: v1.0.0
* @author: LIULJ
* @date: 2018年4月16日 下午8:42:28 
*
* Modification History:
* Date         Author          Version            Description
*---------------------------------------------------------*
* 2018年4月16日     LIULJ           v1.0.0               初始创建
*
*
*/
public class DynaResultSetCallable implements IResultSetCallable<DynaBean> {

	@Override
	public DynaBean invoke(ResultSet rs) throws SQLException {
		DynaBean dataBean = null;
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        dataBean = new DynaBean();
        for (int i = 0; i < columnCount; i++) {
            String name = rsmd.getColumnName(i + 1);
            Object value = rs.getObject(name);
            dataBean.set(name, value);
        }
		return dataBean;
	}

}
