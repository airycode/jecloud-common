/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.doc;

/**
 * 平台文件类型
 * @author zhangshuaipeng
 */
public class JEFileType {
    /**
     * 平台类型  未指定默认平台
     */
    public static String PLATFORM="PLATFORM";
//    /**
//     * JE后端Java
//     */
//    public static String JEBACK="JEBACK";
//    /**
//     * JE前端资源
//     */
//    public static String JEFRONT="JEFRONT";
//    /**
//     * JE前后端共同使用
//     */
//    public static String JECOMMON="JECOMMON";
    /**
     * 项目文件
     */
    public static String PROJECT="PROJECT";
//    /**
//     * JE后端Java源码
//     */
//    public static String JEJAVASOURCE="JEJAVASOURCE";
    /**
     * JE后端Java编译
     */
    public static String JEJAVACLASS="JEJAVACLASS";
}
