/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.workflow.vo;

import java.util.ArrayList;
import java.util.List;

public class CirculationHistoryVo {
    /**
     * 节点名称
     */
    public String nodeName;
    /**
     * 节点名称
     */
    public String nodeId;
    /**
     * 节点类型
     */
    public String nodeType;
    /**
     * 处理人
     */
    public List<String> assignees;
    /**
     * 操作类型
     */
    public String operationType;
    /**
     * 操作类型
     */
    public OperationTypeEnum operationTypeEnum;
    /**
     * 状态
     */
    public CirculationHistoryVoStateEnum state;

    /**
     * 状态
     */
    public String stateName;

    /**
     * 接收时间
     */
    public String startTime;
    /**
     * 审批时间
     */
    public String endTime = "";
    /**
     * 耗时
     */
    public String durationInMillis;
    /**
     * 标题
     */
    private String title;
    /**
     * 是否展示标题
     */
    private String isShowTitle = "0";
    /**
     * 审批意见详情
     */
    private List<CommentVo> comments = new ArrayList<>();

    public static CirculationHistoryVo build(String nodeType) {
        return new CirculationHistoryVo().setNodeType(nodeType);
    }

    public String getNodeName() {
        return nodeName;
    }

    public CirculationHistoryVo setNodeName(String nodeName) {
        this.nodeName = nodeName;
        return this;
    }

    public List<String> getAssignees() {
        return assignees;
    }

    public CirculationHistoryVo setAssignees(List<String> assignees) {
        this.assignees = assignees;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public CirculationHistoryVo setOperationType(String operationType) {
        this.operationType = operationType;
        this.operationTypeEnum = OperationTypeEnum.getTypeByName(operationType);
        return this;
    }

    public CirculationHistoryVoStateEnum getState() {
        return state;
    }

    public CirculationHistoryVo setState(CirculationHistoryVoStateEnum state) {
        this.state = state;
        this.stateName = state.getName();
        return this;
    }

    public String getStartTime() {
        return startTime;
    }

    public CirculationHistoryVo setStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public CirculationHistoryVo setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getDurationInMillis() {
        return durationInMillis;
    }

    public CirculationHistoryVo setDurationInMillis(String durationInMillis) {
        this.durationInMillis = durationInMillis;
        return this;
    }

    public String getNodeType() {
        return nodeType;
    }

    public CirculationHistoryVo setNodeType(String nodeType) {
        this.nodeType = nodeType;
        return this;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsShowTitle() {
        return isShowTitle;
    }

    public void setIsShowTitle(String isShowTitle) {
        this.isShowTitle = isShowTitle;
    }

    public List<CommentVo> getComments() {
        return comments;
    }

    public void setComments(List<CommentVo> comments) {
        this.comments = comments;
    }

    public void addComments(List<CommentVo> commentVos) {
        this.comments.addAll(commentVos);
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void addComment(CommentVo commentVo) {
        this.comments.add(commentVo);
    }

    public OperationTypeEnum getOperationTypeEnum() {
        return operationTypeEnum;
    }

    public void setOperationTypeEnum(OperationTypeEnum operationTypeEnum) {
        this.operationTypeEnum = operationTypeEnum;
    }
}
