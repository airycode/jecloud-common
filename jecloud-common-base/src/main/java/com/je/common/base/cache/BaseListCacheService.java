/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

import java.util.List;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-03-13 11:10
 * @description: 列表缓存服务定义
 */
public interface BaseListCacheService<T> extends BaseContainerCache<T> {

    T leftPopCacheValue();

    T leftPopCacheValue(String tenantId);

    T rightPopCacheValue();

    T rightPopCacheValue(String tenantId);

    void leftPut(T value);

    void leftPut(String tenantId,T value);

    void leftPut(List<T> values);

    void leftPut(String tenantId, List<T> values);

    void rightPut(T value);

    void rightPut(String tenantId,T value);

    void rightPut(List<T> values);

    void rightPut(String tenantId, List<T> values);

    List<T> getAllValues();

}
