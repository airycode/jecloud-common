/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.db;

import com.je.common.base.DynaBean;
import com.je.common.base.entity.QueryInfo;
import com.je.core.entity.extjs.JSONTreeNode;
import java.util.List;

public interface PcDBMethodService {

    /**
	 * 构建查询同步树的sql语句
	 * @param template TODO 暂不明确
	 * @param queryInfo TODO 暂不明确
	 * @param tableName 表名称
	 * @param rootId 根节点id
	 * @return
	 */
	String getTreeSql(JSONTreeNode template, QueryInfo queryInfo, String tableName, String rootId);

	/**
	 * 构建DynaBean查询同步树的sql语句
	 * @param template
	 * @param queryInfo
	 * @param tableName 表名称
	 * @param rootId 根节点id
	 * @return
	 */
	String getDynaTreeSql(List<DynaBean> columns, JSONTreeNode template, String tableName, String rootId, QueryInfo queryInfo);

	/**
	 * 构建查询异步树的sql语句
	 * @param template TODO 暂不明确
	 * @param queryInfo TODO 暂不明确
	 * @param tableName 表名称
	 * @param rootId 根节点id
	 * @param isRoot TODO 暂不明确
	 * @param onlyWhereSql TODO 暂不明确
	 * @return
	 */
	String getAsynTreeSql(JSONTreeNode template, QueryInfo queryInfo, String tableName, String rootId, Boolean isRoot, Boolean onlyWhereSql);

	/**
	 * 查询树形的count
	 * @param template TODO 暂不明确
	 * @param queryInfo TODO 暂不明确
	 * @param tableName 表名称
	 * @param rootId 根节点id
	 * @return
	 */
	String getAsynTreeCount(JSONTreeNode template, QueryInfo queryInfo, String tableName, String rootId);

	/**
	 * 获取角色权限查询sql
	 * @param rootId 根节点
	 * @return
	 */
	String getRolePermLikeSql(String rootId);

	/**
	 * 获取角色权限查询sql
	 * @param rootId 根节点
	 * @return
	 */
	String getRolePermSql(String rootId);

	/**
	 * 获取角色组查询SQL
	 * @param rootId  根节点
	 * @return
	 */
	String getRoleGroupPermLikeSql(String rootId);

	/**
	 * 获取角色组查询SQL
	 * @param rootId 根节点
	 * @return
	 */
	String getRoleGroupPermSql(String rootId);

	/**
	 * 获取数据生成UUID函数
	 * @return
	 */
	String getGenerateUUID();

	/**
	 *    获取截取字符串函数
	 * @return
	 */
	String getSubString();

	/**
	 *  获取字符串长度函数
	 * @return
	 */
	String getLength();

	/**
	 * 获取数据库修改视图语句
	 * @return
	 */
	String getUpdateView();
}
