/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.rbac;

/**
 * @author chenmeng
 * 2012-3-12 下午2:46:24
 */
public class AdminPermType {

	/**
	 * 菜单权限
	 */
	public static final String RES_MENU = "RES_MENU";

	/**
	 * 子功能权限
	 */
	public static final String RES_SUBFUNC = "RES_SUBFUNC";

	/**
	 * 角色权限
	 */
	public static final String ROLE_PERM ="ROLE_PERM";

	/**
	 * 角色查看权限
	 */
	public static final String ROLE_SELECT ="ROLE_SELECT";

	/**
	 * 角色修改权限
	 */
	public static final String ROLE_UPDATE ="ROLE_UPDATE";
	public static final String DEPT_PERM ="DEPT_PERM";
	public static final String DEPT_SELECT ="DEPT_SELECT";
	public static final String DEPT_UPDATE ="DEPT_UPDATE";
	public static final String SENTRY_PERM ="SENTRY_PERM";
	public static final String SENTRY_SELECT ="SENTRY_SELECT";
	public static final String SENTRY_UPDATE ="SENTRY_UPDATE";
}
