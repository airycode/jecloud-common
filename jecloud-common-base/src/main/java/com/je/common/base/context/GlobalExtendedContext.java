/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.context;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局扩展性上下文，用于平台上下文扩展使用
 */
public class GlobalExtendedContext {

    private static final ThreadLocal<Map<String, String>> EXTENDED_CONTEXT = new ThreadLocal<>();

    /**
     * 设置上下文
     *
     * @param key
     * @param value
     */
    public static void put(String key, String value) {
        Map<String, String> contextMap;
        if (EXTENDED_CONTEXT.get() == null) {
            contextMap = new HashMap<>();
            EXTENDED_CONTEXT.set(contextMap);
        } else {
            contextMap = EXTENDED_CONTEXT.get();
        }
        contextMap.put(key, value);
    }

    /**
     * 设置上下文
     *
     * @param mapValues
     */
    public static void putAll(Map<String,String> mapValues) {
        Map<String, String> contextMap;
        if (EXTENDED_CONTEXT.get() == null) {
            contextMap = new HashMap<>();
            EXTENDED_CONTEXT.set(contextMap);
        } else {
            contextMap = EXTENDED_CONTEXT.get();
        }
        contextMap.putAll(mapValues);
    }

    /**
     * 移除上下文key
     *
     * @param keys
     */
    public static void removeContext(String... keys) {
        if (EXTENDED_CONTEXT.get() == null) {
            return;
        }
        for (String eachKey : keys) {
            EXTENDED_CONTEXT.get().remove(eachKey);
        }
    }

    /**
     * 获取上下文Key
     * @param key
     * @return
     */
    public static Object getContextKey(String key) {
        if (EXTENDED_CONTEXT.get() == null) {
            return null;
        }
        return EXTENDED_CONTEXT.get().get(key);
    }

    /**
     * 清空上下文
     */
    public static void clearContext() {
        if (EXTENDED_CONTEXT.get() == null) {
            return;
        }
        EXTENDED_CONTEXT.get().clear();
    }

}
