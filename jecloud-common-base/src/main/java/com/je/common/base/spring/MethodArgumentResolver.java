/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.spring;

import com.google.common.base.Strings;
import com.je.common.base.mvc.HttpMethodArgument;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MethodArgumentResolver implements HandlerMethodArgumentResolver {
    /**
     * TODO 暂不明确
     *
     * @param parameter
     * @return
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        if (parameter.getParameterType().equals(HttpMethodArgument.class)) {
            return true;
        }
        return false;
    }

    /**
     * TODO 暂不明确
     *
     * @param parameter
     * @param mavContainer
     * @param webRequest
     * @param binderFactory
     * @return
     * @throws Exception
     */
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest
            , WebDataBinderFactory binderFactory) throws Exception {

        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        HttpServletResponse response = (HttpServletResponse) webRequest.getNativeResponse();
        //转换参数
        return resolve(request, response);
    }

    /**
     * 转换统一参数
     *
     * @param request  请求对象
     * @param response 响应对象
     * @return
     */
    public static HttpMethodArgument resolve(HttpServletRequest request, HttpServletResponse response) throws Exception {

        HttpMethodArgument param = new HttpMethodArgument();
        param.setRequest(request);
        param.setResponse(response);

        //绑定请求参数
        org.apache.commons.beanutils.BeanUtils.populate(param, request.getParameterMap());

        //原有逻辑只会传 start,limit 改造后需要page,limit。此处做转换
        if (param.getLimit() != -1) {
            param.setPage(param.getStart() / param.getLimit() + 1);
        }

        //查询条件
        String j_query = request.getParameter("j_query");
        if (!Strings.isNullOrEmpty(j_query)) {
            param.setjQuery(j_query);
        }

        //得到表的Code
        String tableCode = request.getParameter("tableCode");
        //赋值查询条件和排序条件
        String whereSql = request.getParameter("whereSql");
        if (Strings.isNullOrEmpty(whereSql)) {
            whereSql = "";
        }
        String orderSql = request.getParameter("orderSql");
        if (Strings.isNullOrEmpty(orderSql)) {
            orderSql = "";
        }

		/*String modelName = request.getParameter("modelName");
		if(StringUtil.isNotEmpty(modelName)) {
			tableCode = modelName;
		}*/
        //研发部:云凤程
        if (Strings.isNullOrEmpty(tableCode)) {
            if (tableCode != null) {
                //logger.error("tableCode is null!");
            }
            return param;
        }
        param.setTableCode(tableCode);

        return param;
    }
}
