/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.entity.doc;

/**
 * 文件保存类型
 * @author zhangshuaipeng
 */
public class JEFileSaveType {
    /**
     * 默认存储，项目目录下
     */
    public static String DEFAULT="default";
    /**
     * 虚拟硬盘
     */
    public static String VIRTUAL="virtual";
//    /**
//     * 分布式文件FASTDFS存储行为
//     */
//    public static String FASTDFS="fastDFS";
//    /**
//     * 分布式文件HDFS存储行为
//     */
//    public static String HDFS="hdfs";
//
//    /**
//     * aliyun存储行为
//     */
//    public static String ALIYUN="aliyun";
    /**
     * third存储行为  第三方存储，暂时定值为aliyun，等分离环境做好再解决
     */
    public static String THIRD="aliyun";



}
