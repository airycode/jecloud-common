/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.spring.SpringContextHolder;
import org.springframework.context.MessageSource;
import java.util.Locale;

/**
 * 国际化消息工具类
 */
public class MessageUtils {

    /**
     * 获取国际化消息
     * @param code
     * @param args
     * @return
     */
    public static String getMessage(Locale locale, String code, Object... args){
        MessageSource messageSource = SpringContextHolder.getBean(MessageSource.class);
        if(messageSource == null){
            return null;
        }
        return messageSource.getMessage(code,args,locale);
    }

    /**
     * 获取国际化消息
     * @param code
     * @param args
     * @return
     */
    public static String getMessage(String code,Object... args){
        return getMessage(Locale.getDefault(),code,args);
    }

}
