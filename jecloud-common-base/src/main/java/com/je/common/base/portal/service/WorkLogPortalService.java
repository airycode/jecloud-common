/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.portal.service;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.auth.impl.account.Account;
import com.je.common.base.portal.vo.WorkLog;
import java.util.List;
import java.util.Map;

public interface WorkLogPortalService {

    /**
     * 查询一条日志的所有信息
     *
     * @param pkValue
     * @return
     */
    DynaBean queryReportInfo(String pkValue);

    /**
     * 增加日志阅读记录
     *
     * @param pkValue
     * @return
     */
    void insertReportReadLog(String pkValue);


    /**
     * 增加日志点评记录
     *
     * @param pkValue
     * @return
     */
    DynaBean insertReportCommentLog(String pkValue);

    /**
     * 查询日志列表 全部 多表联查SQL直接查出需要数据 再用whereSql过滤
     *
     * @param whereSql
     * @return
     */
    List<Map<String,Object>> queryReportList(String whereSql, String orderSql, int... rowStartAndLimit);
    
    /**
     * 查询日志列表数量 全部 多表联查SQL直接查出需要数据 再用whereSql过滤
     *
     * @param whereSql
     * @return
     */
    Long queryReportListCount(String whereSql, String orderSql);

    /**
     * 查询日志列表 已点评与已评价
     *
     * @param whereSql
     * @return
     */
    List<DynaBean> queryReportList(String whereSql, String orderSql, String isComment, String isRead);

    /**
     * 获取日志主键ID的集合
     *
     * @param dynaBeans 含有日志ID的表
     * @return
     */
    List<String> getReportIds(List<DynaBean> dynaBeans);

    /**
     * 获取日志的badge
     * @param userId
     * @return
     */
    JSONObject getRzNum(String userId);

    /**
     * 获取日志的badge
     * @param userId
     * @return
     */
    JSONObject getRzBadge(String userId);

    /**
     * 保存日志信息
     * @param workLog TODO 暂不明确
     * @return
     */
    Map<String,Object> saveWorkLog(WorkLog workLog);

    /**
     * 修改日志信息
     * @param workLog TODO 暂不明确
     * @return
     */
    Map<String,Object> updateWorkLog(WorkLog workLog);

    /**
     * 查询工作日志
     * @param workLog TODO 暂不明确
     * @param currentUser 当前登陆用户信息
     * @return
     */
    List<Map<String,Object>> findWorkLog(WorkLog workLog, Account currentUser);

    /**
     * 点评日志接口
     * @param workLog  TODO 暂不明确
     * @param currentUser 当前登陆用户信息
     * @return
     */
    Map<String,Object> appraiseWorkLog(WorkLog workLog, Account currentUser);

    /**
     * 添加修改日志
     * @param dynaBean  TODO 暂不明确
     * @return
     */
    DynaBean saveTask(DynaBean dynaBean);

}
