/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.table;

/**
 * 字段表列的数据类型
 * @author zhangshuaipeng
 */
public class ColumnType {
	/**主键ID类型*/
	public static final String ID = "ID";
	/**字符串255位数*/
	public static final String VARCHAR30 = "VARCHAR30";
	/**字符串255位数*/
	public static final String VARCHAR50 = "VARCHAR50";
	/**字符串255位数*/
	public static final String VARCHAR100 = "VARCHAR100";
	/**字符串255位数*/
	public static final String VARCHAR255 = "VARCHAR255";
	/**字符串767*/
	public static final String VARCHAR767 = "VARCHAR767";
	/**字符串1000*/
	public static final String VARCHAR1000 = "VARCHAR1000";
	/**字符串2000*/
	public static final String VARCHAR2000 = "VARCHAR2000";
	/**字符串4000*/
	public static final String VARCHAR4000 = "VARCHAR4000";
	/**自定义长度的字符串*/
	public static final String VARCHAR = "VARCHAR";
	/**整数*/
	public static final String NUMBER = "NUMBER";
	/**日期型*/
	public static final String DATE = "DATE";
	/**日期时间型*/
	public static final String DATETIME = "DATETIME";
	/**小数*/
	public static final String FLOAT = "FLOAT";
	/**小数2*/
	public static final String FLOAT2 = "FLOAT2";
	/**是非类型*/
	public static final String YESORNO = "YESORNO";
	/**大文本型*/
	public static final String CLOB = "CLOB";
	/**巨大文本型*/
	public static final String BIGCLOB = "BIGCLOB";
	/**外键*/ 
	public static final String FOREIGNKEY = "FOREIGNKEY";
	/**自定义*/
	public static final String CUSTOM = "CUSTOM";
	/**外键自定义*/
	public static final String CUSTOMFOREIGNKEY = "CUSTOMFOREIGNKEY";
	/**主键自定义*/
	public static final String CUSTOMID = "CUSTOMID";

}
