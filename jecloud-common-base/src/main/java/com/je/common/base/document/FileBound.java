/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.document;

/**
 * @program: je-document-boot
 * @author: LIULJ
 * @create: 2020/3/10
 * @description:
 */
public class FileBound extends BasePojo {

    /**
     * 预设文件唯一标识（当此标识存在数据库中时，不会执行文件上传操作）
     */
    private String fileKey;

    /**
     * 文件名称(含后缀)
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String contentType;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 文件夹路径
     */
    private String folder;

    /**
     * 文件全路径
     */
    private String fullUrl;

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    /**
     *
     * @param fileKey 文件唯一键值
     * @param fileName 文件名称
     * @param contentType 文件类型
     * @param size 文件大小
     * @param folder 文件夹路径
     */
    public FileBound(String fileKey, String fileName, String contentType, Long size, String folder) {
        this.fileKey = fileKey;
        this.fileName = fileName;
        this.contentType = contentType;
        this.size = size;
        this.folder = folder;
        this.fullUrl = fullUrl;
    }

    /**
     *
     * @param fileKey 文件唯一键值
     * @param fileName 文件名称
     * @param contentType 文件类型
     * @param size 文件大小
     * @param folder 文件夹路径
     * @param fullUrl 文件全路径
     */
    public FileBound(String fileKey, String fileName, String contentType, Long size, String folder, String fullUrl) {
        this.fileKey = fileKey;
        this.fileName = fileName;
        this.contentType = contentType;
        this.size = size;
        this.folder = folder;
        this.fullUrl = fullUrl;
    }

    @Override
    public String toString() {
        return "FileBound{" +
                "fileKey='" + fileKey + '\'' +
                ", fileName='" + fileName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", size=" + size +
                ", folder='" + folder + '\'' +
                ", fullUrl='" + fullUrl + '\'' +
                '}';
    }
}
