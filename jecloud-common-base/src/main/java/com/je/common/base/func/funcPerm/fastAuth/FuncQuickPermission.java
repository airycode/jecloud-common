/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm.fastAuth;

import java.io.Serializable;
import java.util.List;

/**
 * 功能快速授权
 */
public class FuncQuickPermission implements Serializable {

    private boolean active;
    //------------------------------本人------------------------------
    /**
     * 本人修改本人
     */
    private boolean myselfEdit;
    /**
     * 本人删除本人
     */
    private boolean myselfDelete;

    //------------------------------部门主管------------------------------
    /**
     * 部门主管可见
     */
    private boolean deptHeadQuery;
    /**
     * 部门主管可以修改
     */
    private boolean deptHeadEdit;
    /**
     * 部门主管可以删除
     */
    private boolean deptHeadDelete;

    //------------------------------直属领导------------------------------
    /**
     * 直属领导可见
     */
    private boolean directLeaderQuery;
    /**
     * 直属领导可以修改
     */
    private boolean directLeaderEdit;
    /**
     * 直属领导可以删除
     */
    private boolean directLeaderDelete;

    //------------------------------监管部门领导------------------------------
    /**
     * 监管部门领导可见
     */
    private boolean monitorLeaderQuery;
    /**
     * 监管部门领导修改
     */
    private boolean monitorLeaderEdit;
    /**
     * 监管部门领导删除
     */
    private boolean monitorLeaderDelete;

    //------------------------------本部门内------------------------------
    /**
     * 本部门内可见
     */
    private boolean myDeptQuery;
    /**
     * 本部门内修改
     */
    private boolean myDeptEdit;
    /**
     * 本部门内删除
     */
    private boolean myDeptDelete;

    //------------------------------部门监管部门可见------------------------------
    /**
     * 部门监管部门可见
     */
    private boolean deptMonitorDeptQuery;
    /**
     * 部门监管部门修改
     */
    private boolean deptMonitorDeptEdit;
    /**
     * 部门监管删除
     */
    private boolean deptMonitorDeptDelete;

    //------------------------------待审批可见------------------------------
    /**
     * 待审批可见
     */
    private boolean approPreQuery;
    /**
     * 待审批修改
     */
    private boolean approvePendingUpdate;
    /**
     * 待审批删除
     */
    private boolean approvePendingDelete;

    //------------------------------审批后可见------------------------------
    /**
     * 审批后可见
     */
    private boolean approAfterQuery;
    /**
     * 审批后修改
     */
    private boolean approvePendedUpdate;
    /**
     * 审批后删除
     */
    private boolean approvePendedDelete;

    //------------------------------公司内可见------------------------------
    /**
     * 公司内可见
     */
    private boolean myCompanyQuery;
    /**
     * 公司内修改
     */
    private boolean myCompanyEdit;
    /**
     * 公司内删除
     */
    private boolean myCompanyDelete;

    //------------------------------集团公司可见------------------------------
    /**
     * 集团公司内可见
     */
    private boolean groupCompanyQuery;
    /**
     * 集团公司内修改
     */
    private boolean groupCompanyEdit;
    /**
     * 集团公司内删除
     */
    private boolean groupCompanyDelete;

    //------------------------------公司监管------------------------------
    /**
     * 公司监管可见
     */
    private boolean monitorCompanyQuery;
    /**
     * 公司监管修改
     */
    private boolean monitorCompanyEdit;
    /**
     * 公司监管删除
     */
    private boolean monitorCompanyDelete;

    /**
     * 可见人员
     */
    private boolean userControlQuery;
    /**
     * 可见人员
     */
    private List<String> seeUserIdList;
    /**
     * 可见人员
     */
    private boolean userControlEdit;
    /**
     * 可见人员
     */
    private boolean userControlDelete;
    /**
     * 可见部门
     */
    private boolean deptControlQuery;
    /**
     * 可见部门
     */
    private List<String> seeDeptIdList;
    /**
     * 可见部门
     */
    private boolean deptControlEdit;
    /**
     * 可见部门
     */
    private boolean deptControlDelete;
    /**
     * 可见角色
     */
    private boolean roleControlQuery;
    /**
     * 可见角色
     */
    private List<String> seeRoleIdList;
    /**
     * 可见角色
     */
    private boolean roleControlEdit;
    /**
     * 可见角色
     */
    private boolean roleControlDelete;
    /**
     * 可见机构
     */
    private boolean orgControlQuery;
    /**
     * 可见机构
     */
    private List<String> seeOrgIdList;
    /**
     * 可见机构
     */
    private boolean orgControlEdit;
    /**
     * 可见机构
     */
    private boolean orgControlDelete;
    /**
     * 创建用户ID字段
     */
    private String createUserIdField = "SY_CREATEUSERID";
    /**
     * 创建部门ID字段
     */
    private String createDeptIdField = "SY_CREATEORGID";
    /**
     * 登记公司ID字段编码
     */
    private String companyIdFieldCode = "SY_COMPANY_ID";
    /**
     * 登记集团公司ID字段编码
     */
    private String groupCompanyIdFieldCode ="SY_GROUP_COMPANY_ID";

    public String getCompanyIdFieldCode() {
        return companyIdFieldCode;
    }

    public void setCompanyIdFieldCode(String companyIdFieldCode) {
        this.companyIdFieldCode = companyIdFieldCode;
    }

    public String getGroupCompanyIdFieldCode() {
        return groupCompanyIdFieldCode;
    }

    public void setGroupCompanyIdFieldCode(String groupCompanyIdFieldCode) {
        this.groupCompanyIdFieldCode = groupCompanyIdFieldCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isMyselfEdit() {
        return myselfEdit;
    }

    public void setMyselfEdit(boolean myselfEdit) {
        this.myselfEdit = myselfEdit;
    }

    public boolean isMyselfDelete() {
        return myselfDelete;
    }

    public void setMyselfDelete(boolean myselfDelete) {
        this.myselfDelete = myselfDelete;
    }

    public boolean isDeptHeadQuery() {
        return deptHeadQuery;
    }

    public void setDeptHeadQuery(boolean deptHeadQuery) {
        this.deptHeadQuery = deptHeadQuery;
    }

    public boolean isDeptHeadEdit() {
        return deptHeadEdit;
    }

    public void setDeptHeadEdit(boolean deptHeadEdit) {
        this.deptHeadEdit = deptHeadEdit;
    }

    public boolean isDeptHeadDelete() {
        return deptHeadDelete;
    }

    public void setDeptHeadDelete(boolean deptHeadDelete) {
        this.deptHeadDelete = deptHeadDelete;
    }

    public boolean isDirectLeaderQuery() {
        return directLeaderQuery;
    }

    public void setDirectLeaderQuery(boolean directLeaderQuery) {
        this.directLeaderQuery = directLeaderQuery;
    }

    public boolean isDirectLeaderEdit() {
        return directLeaderEdit;
    }

    public void setDirectLeaderEdit(boolean directLeaderEdit) {
        this.directLeaderEdit = directLeaderEdit;
    }

    public boolean isDirectLeaderDelete() {
        return directLeaderDelete;
    }

    public void setDirectLeaderDelete(boolean directLeaderDelete) {
        this.directLeaderDelete = directLeaderDelete;
    }

    public boolean isMonitorLeaderQuery() {
        return monitorLeaderQuery;
    }

    public void setMonitorLeaderQuery(boolean monitorLeaderQuery) {
        this.monitorLeaderQuery = monitorLeaderQuery;
    }

    public boolean isMonitorLeaderEdit() {
        return monitorLeaderEdit;
    }

    public void setMonitorLeaderEdit(boolean monitorLeaderEdit) {
        this.monitorLeaderEdit = monitorLeaderEdit;
    }

    public boolean isMonitorLeaderDelete() {
        return monitorLeaderDelete;
    }

    public void setMonitorLeaderDelete(boolean monitorLeaderDelete) {
        this.monitorLeaderDelete = monitorLeaderDelete;
    }

    public boolean isMyDeptQuery() {
        return myDeptQuery;
    }

    public void setMyDeptQuery(boolean myDeptQuery) {
        this.myDeptQuery = myDeptQuery;
    }

    public boolean isMyDeptEdit() {
        return myDeptEdit;
    }

    public void setMyDeptEdit(boolean myDeptEdit) {
        this.myDeptEdit = myDeptEdit;
    }

    public boolean isMyDeptDelete() {
        return myDeptDelete;
    }

    public void setMyDeptDelete(boolean myDeptDelete) {
        this.myDeptDelete = myDeptDelete;
    }

    public boolean isDeptMonitorDeptQuery() {
        return deptMonitorDeptQuery;
    }

    public void setDeptMonitorDeptQuery(boolean deptMonitorDeptQuery) {
        this.deptMonitorDeptQuery = deptMonitorDeptQuery;
    }

    public boolean isDeptMonitorDeptEdit() {
        return deptMonitorDeptEdit;
    }

    public void setDeptMonitorDeptEdit(boolean deptMonitorDeptEdit) {
        this.deptMonitorDeptEdit = deptMonitorDeptEdit;
    }

    public boolean isDeptMonitorDeptDelete() {
        return deptMonitorDeptDelete;
    }

    public void setDeptMonitorDeptDelete(boolean deptMonitorDeptDelete) {
        this.deptMonitorDeptDelete = deptMonitorDeptDelete;
    }

    public boolean isApproPreQuery() {
        return approPreQuery;
    }

    public void setApproPreQuery(boolean approPreQuery) {
        this.approPreQuery = approPreQuery;
    }

    public boolean isApprovePendingUpdate() {
        return approvePendingUpdate;
    }

    public void setApprovePendingUpdate(boolean approvePendingUpdate) {
        this.approvePendingUpdate = approvePendingUpdate;
    }

    public boolean isApprovePendingDelete() {
        return approvePendingDelete;
    }

    public void setApprovePendingDelete(boolean approvePendingDelete) {
        this.approvePendingDelete = approvePendingDelete;
    }

    public boolean isApproAfterQuery() {
        return approAfterQuery;
    }

    public void setApproAfterQuery(boolean approAfterQuery) {
        this.approAfterQuery = approAfterQuery;
    }

    public boolean isApprovePendedUpdate() {
        return approvePendedUpdate;
    }

    public void setApprovePendedUpdate(boolean approvePendedUpdate) {
        this.approvePendedUpdate = approvePendedUpdate;
    }

    public boolean isApprovePendedDelete() {
        return approvePendedDelete;
    }

    public void setApprovePendedDelete(boolean approvePendedDelete) {
        this.approvePendedDelete = approvePendedDelete;
    }

    public boolean isMyCompanyQuery() {
        return myCompanyQuery;
    }

    public void setMyCompanyQuery(boolean myCompanyQuery) {
        this.myCompanyQuery = myCompanyQuery;
    }

    public boolean isMyCompanyEdit() {
        return myCompanyEdit;
    }

    public void setMyCompanyEdit(boolean myCompanyEdit) {
        this.myCompanyEdit = myCompanyEdit;
    }

    public boolean isMyCompanyDelete() {
        return myCompanyDelete;
    }

    public void setMyCompanyDelete(boolean myCompanyDelete) {
        this.myCompanyDelete = myCompanyDelete;
    }

    public boolean isGroupCompanyQuery() {
        return groupCompanyQuery;
    }

    public void setGroupCompanyQuery(boolean groupCompanyQuery) {
        this.groupCompanyQuery = groupCompanyQuery;
    }

    public boolean isGroupCompanyEdit() {
        return groupCompanyEdit;
    }

    public void setGroupCompanyEdit(boolean groupCompanyEdit) {
        this.groupCompanyEdit = groupCompanyEdit;
    }

    public boolean isGroupCompanyDelete() {
        return groupCompanyDelete;
    }

    public void setGroupCompanyDelete(boolean groupCompanyDelete) {
        this.groupCompanyDelete = groupCompanyDelete;
    }

    public boolean isMonitorCompanyQuery() {
        return monitorCompanyQuery;
    }

    public void setMonitorCompanyQuery(boolean monitorCompanyQuery) {
        this.monitorCompanyQuery = monitorCompanyQuery;
    }

    public boolean isMonitorCompanyEdit() {
        return monitorCompanyEdit;
    }

    public void setMonitorCompanyEdit(boolean monitorCompanyEdit) {
        this.monitorCompanyEdit = monitorCompanyEdit;
    }

    public boolean isMonitorCompanyDelete() {
        return monitorCompanyDelete;
    }

    public void setMonitorCompanyDelete(boolean monitorCompanyDelete) {
        this.monitorCompanyDelete = monitorCompanyDelete;
    }

    public boolean isUserControlQuery() {
        return userControlQuery;
    }

    public void setUserControlQuery(boolean userControlQuery) {
        this.userControlQuery = userControlQuery;
    }

    public List<String> getSeeUserIdList() {
        return seeUserIdList;
    }

    public void setSeeUserIdList(List<String> seeUserIdList) {
        this.seeUserIdList = seeUserIdList;
    }

    public boolean isUserControlEdit() {
        return userControlEdit;
    }

    public void setUserControlEdit(boolean userControlEdit) {
        this.userControlEdit = userControlEdit;
    }

    public boolean isUserControlDelete() {
        return userControlDelete;
    }

    public void setUserControlDelete(boolean userControlDelete) {
        this.userControlDelete = userControlDelete;
    }

    public boolean isDeptControlQuery() {
        return deptControlQuery;
    }

    public void setDeptControlQuery(boolean deptControlQuery) {
        this.deptControlQuery = deptControlQuery;
    }

    public List<String> getSeeDeptIdList() {
        return seeDeptIdList;
    }

    public void setSeeDeptIdList(List<String> seeDeptIdList) {
        this.seeDeptIdList = seeDeptIdList;
    }

    public boolean isDeptControlEdit() {
        return deptControlEdit;
    }

    public void setDeptControlEdit(boolean deptControlEdit) {
        this.deptControlEdit = deptControlEdit;
    }

    public boolean isDeptControlDelete() {
        return deptControlDelete;
    }

    public void setDeptControlDelete(boolean deptControlDelete) {
        this.deptControlDelete = deptControlDelete;
    }

    public boolean isRoleControlQuery() {
        return roleControlQuery;
    }

    public void setRoleControlQuery(boolean roleControlQuery) {
        this.roleControlQuery = roleControlQuery;
    }

    public List<String> getSeeRoleIdList() {
        return seeRoleIdList;
    }

    public void setSeeRoleIdList(List<String> seeRoleIdList) {
        this.seeRoleIdList = seeRoleIdList;
    }

    public boolean isRoleControlEdit() {
        return roleControlEdit;
    }

    public void setRoleControlEdit(boolean roleControlEdit) {
        this.roleControlEdit = roleControlEdit;
    }

    public boolean isRoleControlDelete() {
        return roleControlDelete;
    }

    public void setRoleControlDelete(boolean roleControlDelete) {
        this.roleControlDelete = roleControlDelete;
    }

    public boolean isOrgControlQuery() {
        return orgControlQuery;
    }

    public void setOrgControlQuery(boolean orgControlQuery) {
        this.orgControlQuery = orgControlQuery;
    }

    public List<String> getSeeOrgIdList() {
        return seeOrgIdList;
    }

    public void setSeeOrgIdList(List<String> seeOrgIdList) {
        this.seeOrgIdList = seeOrgIdList;
    }

    public boolean isOrgControlEdit() {
        return orgControlEdit;
    }

    public void setOrgControlEdit(boolean orgControlEdit) {
        this.orgControlEdit = orgControlEdit;
    }

    public boolean isOrgControlDelete() {
        return orgControlDelete;
    }

    public void setOrgControlDelete(boolean orgControlDelete) {
        this.orgControlDelete = orgControlDelete;
    }

    public String getCreateUserIdField() {
        return createUserIdField;
    }

    public void setCreateUserIdField(String createUserIdField) {
        this.createUserIdField = createUserIdField;
    }

    public String getCreateDeptIdField() {
        return createDeptIdField;
    }

    public void setCreateDeptIdField(String createDeptIdField) {
        this.createDeptIdField = createDeptIdField;
    }
}
