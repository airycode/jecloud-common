/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.workflow;

public class WfProcessVarType {
	/** 流程定义创建人 */
	public static final String PI_OWNER = "PI_OWNER";
	/** 流程定义业务主键 */
	public static final String PI_PDID = "PI_PDID";
	/** 流程定义业务主键 */
	public static final String PI_PKVALUE = "PI_PKVALUE";
	/** 历史信息上一执行节点 */
	public static final String PI_BEFORETASK="PI_BEFORETASK";
	/** 取回人 */
	public static final String PI_RETURNASSGINE="PI_RETURNASSGINE";
	/** 目标路径 */
	public static final String PI_TARGER_TRANSITION="PI_TARGER_TRANSITION";
	/**已审批人*/
	public static final String PI_APPROVED="PI_APPROVED";
	/**延迟任务*/
	public static final String PI_DELAY="PI_DELAY";
	/**待审批人*/
	public static final String PI_PREAPPROV="PI_PREAPPROV";
	/**审批会签信息*/
	public static final String PI_COUNTERSIGN="PI_COUNTERSIGN";
	/**历史信息*/
	public static final String PI_HISTORY="PI_HISTORY";
	/**审批会签信息*/
	public static final String PI_FORK="PI_FORK";
	/**多人审批信息*/
	public static final String PI_BATCH="PI_BATCH";
	/**系统自动通过节点指派人，历史记录不展示*/
	public static final String PI_SYS_AUTOUSER="PI_SYS_AUTOUSER";
	/**系统自动节点执行人，虚拟用户*/
	public static final String PI_SYS_AUTONODE_ASSGINE="PI_SYS_AUTONODE_ASSGINE";
	/**系统自动通过节点指派人，历史记录不展示*/
	public static final String PI_JOIN_SIZE="PI_JOIN_SIZE";
}
