/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.impl;

import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.CommonCheckService;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import com.je.common.base.DynaBean;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonCheckServiceImpl implements CommonCheckService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaResourceService metaResourceService;

    private static final String WHERESQL = "and ";
    private static final String ORDERSQL = "order by ";

    @Override
    public boolean checkRepeat(ConditionsWrapper conditionsWrapper) {
        List<DynaBean> list = metaService.select(conditionsWrapper);
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public String checkRepeatData(DynaBean dynaBean, Boolean isUpdate) {
        DynaBean funcInfo = metaResourceService.selectOneByNativeQuery("JE_CORE_FUNCINFO", NativeQuery.build().eq("FUNCINFO_FUNCCODE", dynaBean.getStr("funcCode")));
        List<DynaBean> list = metaResourceService.selectByTableCodeAndNativeQuery("JE_CORE_RESOURCEFIELD", NativeQuery.build()
                .eq("RESOURCEFIELD_VTYPE", "unique").eq("RESOURCEFIELD_FUNCINFO_ID", funcInfo.getStr("JE_CORE_FUNCINFO_ID")));
        for (DynaBean item : list) {
            String fieldValue = dynaBean.getStr(item.getStr("RESOURCEFIELD_CODE"));
            if (StringUtil.isNotEmpty(fieldValue)) {
                ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
                conditionsWrapper.eq(item.getStr("RESOURCEFIELD_CODE"), fieldValue);
                if (isUpdate) {
                    conditionsWrapper.eq(dynaBean.getPkCode(), dynaBean.getPkValue());
                }
                List<DynaBean> userData = metaService.select(dynaBean.getTableCode(), conditionsWrapper);
                if (userData != null && userData.size() > 0) {
                    return "";
                }
            }
        }
        return null;
    }

    @Override
    public boolean checkSql(String tableCode, String whereSql, String orderSql) {
        String sqlTemplate = " SELECT * FROM " + tableCode + " ";
        if (StringUtil.isNotEmpty(whereSql)) {
            sqlTemplate += " WHERE " + whereSql + " ";
        }
        if (StringUtil.isNotEmpty(orderSql)) {
            sqlTemplate += " ORDER BY " + orderSql;
        }
        try {
            metaService.selectSql(sqlTemplate);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
