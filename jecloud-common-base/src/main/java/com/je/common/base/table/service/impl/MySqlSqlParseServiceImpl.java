/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service.impl;

import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.je.common.base.table.service.SqlParseService;
import com.je.common.base.util.SQLFormatterUtil;
import org.springframework.stereotype.Service;

import java.sql.SQLSyntaxErrorException;

@Service("mySqlSqlParseService")
public class MySqlSqlParseServiceImpl extends AbstractSqlParseServiceImpl implements SqlParseService {

    private static final String dbType = "mysql";

    @Override
    public SQLSelectStatement selectParse(String selectSql) throws SQLSyntaxErrorException {
        return super.selectParse(selectSql, dbType);
    }

    @Override
    public String beautifySelect(String sql) throws SQLSyntaxErrorException {
        SQLSelectStatement statement = selectParse(sql, dbType);
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLAlterStatement alertParse(String selectSql) throws SQLSyntaxErrorException {
        return super.alertParse(selectSql, dbType);
    }

    @Override
    public SQLInsertStatement insertParse(String sql) throws SQLSyntaxErrorException {
        return super.insertParse(sql, dbType);
    }

    @Override
    public String beautifyInsert(String sql) throws SQLSyntaxErrorException {
        SQLInsertStatement statement = insertParse(sql, dbType);
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLUpdateStatement updateParse(String sql) throws SQLSyntaxErrorException {
        return super.updateParse(sql, dbType);
    }

    @Override
    public String beautifyUpdate(String sql) throws SQLSyntaxErrorException {
        SQLUpdateStatement statement = updateParse(sql, dbType);
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLCreateTableStatement createTableParse(String createTableDdl) throws SQLSyntaxErrorException {
        return super.createTableParse(createTableDdl, dbType);
    }

    @Override
    public String beautifyCreateTableDdl(String ddl) throws SQLSyntaxErrorException {
        SQLCreateTableStatement statement = createTableParse(ddl, dbType);
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public String beautifyAlertTableDdl(String ddl) throws SQLSyntaxErrorException {
        SQLAlterStatement statement = alertParse(ddl, dbType);
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        return statement.toString();
    }

    @Override
    public SQLCreateViewStatement createViewParse(String createViewDdl) throws SQLSyntaxErrorException {
        return super.createViewParse(createViewDdl, dbType);
    }

    @Override
    public String beautifyCreateViewDdl(String ddl) throws SQLSyntaxErrorException {
//        SQLCreateViewStatement statement = createViewParse(ddl, dbType);
//        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
//        statement.accept(visitor);
        return SQLFormatterUtil.format(ddl).trim();
    }

    public static void main(String[] args) throws SQLSyntaxErrorException {
        SqlParseService sqlParseService = new MySqlSqlParseServiceImpl();
//        String sql = sqlParseService.beautifySelect("select * from je_core_enduser where userid='1' and usercode='admin'");
//        System.out.println(sql);
        String ddl = "CREATE VIEW YZST2 AS SELECT JEC_DYZB.JEC_DYZB_ID AS JEC_DYZB_ID, JEC_DYZB.DYZB_LDHTLX_CODE AS DYZB_LDHTLX_CODE, JEC_DYZB.DYZB_ZD2 AS DYZB_ZD2, JEC_DYZB.DYZB_LDHTLX_NAME AS DYZB_LDHTLX_NAME, JEC_DYZB.DYZB_ZD41 AS DYZB_ZD41\n" +
                ", JEC_DEZB.DEZB_ZDF AS DEZB_ZDF, JEC_DEZB.DEZB_ZDS AS DEZB_ZDS, JEC_DYZB.DYZB_ZD3 AS DYZB_ZD3 FROM JEC_DYZB, JEC_DEZB WHERE 1 = 1 " +
                "AND JEC_DYZB.JEC_DYZB_ID = JEC_DEZB.JEC_DEZB_ID";
        String beautifyDdl = sqlParseService.beautifyCreateViewDdl(ddl);
        System.out.println(beautifyDdl);
    }

}
