/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.portal.vo;

import com.alibaba.fastjson2.JSONObject;

public class PushActVo {
    /**
     * 类型
     */
    private String type;
    /**
     * 编码
     */
    private String code;
    /**
     * 角标
     */
    private Boolean badge=false;
    /**
     * 更新到APP应用外的总角标
     */
    private Boolean apkBadge=false;
    /**
     * 角标数值
     */
    private JSONObject badgeObj=new JSONObject();

    /**
     * 刷新
     */
    private Boolean refresh=false;
    /**
     * 更新数字
     */
    private Boolean num=false;
    /**
     * 更新数字
     */
    private Boolean refreshNum=false;
    /**
     * 数字信息
     */
    private JSONObject numObj=new JSONObject();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getRefresh() {
        return refresh;
    }

    public void setRefresh(Boolean refresh) {
        this.refresh = refresh;
    }

    public Boolean getNum() {
        return num;
    }

    public void setNum(Boolean num) {
        this.num = num;
    }

    public JSONObject getNumObj() {
        return numObj;
    }

    public void setNumObj(JSONObject numObj) {
        this.numObj = numObj;
    }

    public Boolean getRefreshNum() {
        return refreshNum;
    }

    public void setRefreshNum(Boolean refreshNum) {
        this.refreshNum = refreshNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getBadge() {
        return badge;
    }

    public void setBadge(Boolean badge) {
        this.badge = badge;
    }

    public JSONObject getBadgeObj() {
        return badgeObj;
    }

    public void setBadgeObj(JSONObject badgeObj) {
        this.badgeObj = badgeObj;
    }

    public Boolean getApkBadge() {
        return apkBadge;
    }

    public void setApkBadge(Boolean apkBadge) {
        this.apkBadge = apkBadge;
    }
}
