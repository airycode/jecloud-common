/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service;

import com.alibaba.druid.sql.ast.statement.*;

import java.sql.SQLSyntaxErrorException;

/**
 * sql解析服务定义
 */
public interface SqlParseService {

    /**
     * 解析select
     * @param selectSql 查询语句
     * @return
     */
    SQLSelectStatement selectParse(String selectSql) throws SQLSyntaxErrorException;

    /**
     * 解析select
     * @param selectSql 查询语句
     * @param dbType 数据库类型
     * @return
     */
    SQLSelectStatement selectParse(String selectSql,String dbType) throws SQLSyntaxErrorException;

    /**
     * 美化select
     * @param sql
     * @return
     */
    String beautifySelect(String sql) throws SQLSyntaxErrorException;

    /**
     * 解析alert
     * @param selectSql 查询语句
     * @return
     */
    SQLAlterStatement alertParse(String selectSql) throws SQLSyntaxErrorException;

    /**
     * 解析alert
     * @param selectSql 查询语句
     * @param dbType 数据库类型
     * @return
     */
    SQLAlterStatement alertParse(String selectSql,String dbType) throws SQLSyntaxErrorException;

    /**
     * 解析insert
     * @param sql insert语句
     * @return
     */
    SQLInsertStatement insertParse(String sql) throws SQLSyntaxErrorException;

    /**
     * 解析insert
     * @param sql insert语句
     * @return
     */
    String beautifyInsert(String sql) throws SQLSyntaxErrorException;

    /**
     * 解析insert
     * @param sql insert语句
     * @param dbType 数据库类型
     * @return
     */
    SQLInsertStatement insertParse(String sql,String dbType) throws SQLSyntaxErrorException;

    /**
     * 解析Uodate
     * @param sql 更新语句
     * @return
     */
    SQLUpdateStatement updateParse(String sql) throws SQLSyntaxErrorException;

    /**
     * 美化更新语句
     * @param sql
     * @return
     */
    String beautifyUpdate(String sql) throws SQLSyntaxErrorException;

    /**
     * 解析Uodate
     * @param sql 更新语句
     * @param dbType 数据库类型
     * @return
     */
    SQLUpdateStatement updateParse(String sql,String dbType) throws SQLSyntaxErrorException;

    /**
     * 创建表DDL
     * @param createTableDdl 创建表DDL
     * @return
     */
    SQLCreateTableStatement createTableParse(String createTableDdl) throws SQLSyntaxErrorException;

    /**
     * 美化创建表DDL
     * @param ddl
     * @return
     */
    String beautifyCreateTableDdl(String ddl) throws SQLSyntaxErrorException;

    /**
     * 美化alertDDL
     * @param ddl
     * @return
     */
    String beautifyAlertTableDdl(String ddl) throws SQLSyntaxErrorException;

    /**
     * 创建表DDL
     * @param createTableDdl 创建表DDL
     * @param dbType 数据库类型
     * @return
     */
    SQLCreateTableStatement createTableParse(String createTableDdl,String dbType) throws SQLSyntaxErrorException;

    /**
     * DDL解析
     * @param createViewDdl 创建视图DDL
     * @return
     */
    SQLCreateViewStatement createViewParse(String createViewDdl) throws SQLSyntaxErrorException;

    /**
     * 创建视图DDL
     * @param ddl
     * @return
     */
    String beautifyCreateViewDdl(String ddl) throws SQLSyntaxErrorException;

    /**
     * DDL解析
     * @param createViewDdl 创建视图DDL
     * @param dbType 数据库类型
     * @return
     */
    SQLCreateViewStatement createViewParse(String createViewDdl,String dbType) throws SQLSyntaxErrorException;
}
