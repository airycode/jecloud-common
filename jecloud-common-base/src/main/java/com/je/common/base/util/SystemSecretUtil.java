/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.context.GlobalExtendedContext;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统密级工具类
 */
public class SystemSecretUtil {

    /**
     * 系统密级开关Key
     */
    public static final String SECRET_SWITCH_KEY = "secretSwitch";
    /**
     * 系统密级编码Key
     */
    public static final String SECRET_CODE_KEY = "secretLevelCode";
    /**
     * 系统密级名称Key
     */
    public static final String SECRET_CODE_NAME = "secretLevelName";

    /**
     * 上下文密级编码Key
     */
    public static final String CONTEXT_SECRET_CODE_KEY = "contextSecretCode";
    /**
     * 上下文密级名称Key
     */
    public static final String CONTEXT_SECRET_CODE_NAME = "contextSecretName";

    /**
     * 是否开启系统密级
     *
     * @return
     */
    public static boolean isOpen() {
        Object value = GlobalExtendedContext.getContextKey(SECRET_SWITCH_KEY);
        if ("1".equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 系统密级编码
     *
     * @return
     */
    public static String systemSecretCode() {
        Object value = GlobalExtendedContext.getContextKey(SECRET_CODE_KEY);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * 系统密级名称
     *
     * @return
     */
    public static String systemSecretName() {
        Object value = GlobalExtendedContext.getContextKey(SECRET_CODE_NAME);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * 用户上下文密级编码
     *
     * @return
     */
    public static String contextSecretCode() {
        Object value = GlobalExtendedContext.getContextKey(CONTEXT_SECRET_CODE_KEY + "_" + SecurityUserHolder.getCurrentAccountId());
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * 用户上下文密级名称
     *
     * @return
     */
    public static String contextSecretName() {
        Object value = GlobalExtendedContext.getContextKey(CONTEXT_SECRET_CODE_NAME + "_" + SecurityUserHolder.getCurrentAccountId());
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * 获取系统密级信息
     *
     * @return
     */
    public static Map<String, String> systemSecretInfo() {
        Object switchValue = GlobalExtendedContext.getContextKey(SECRET_SWITCH_KEY);
        Object codeValue = GlobalExtendedContext.getContextKey(SECRET_CODE_KEY);
        Object nameValue = GlobalExtendedContext.getContextKey(SECRET_CODE_NAME);

        Map<String, String> values = new HashMap<>();
        values.put(SECRET_SWITCH_KEY,switchValue == null ? null:switchValue.toString());
        values.put(SECRET_CODE_KEY,codeValue == null ? null:codeValue.toString());
        values.put(SECRET_CODE_NAME,nameValue == null ? null:nameValue.toString());

        return values;
    }

}
