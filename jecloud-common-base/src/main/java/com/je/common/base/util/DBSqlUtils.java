/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.google.common.base.Strings;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.ExtFieldType;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.entity.extjs.Model;
import com.je.common.base.table.service.*;
import org.apache.commons.lang.StringUtils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 格式化兼容数据库的工具类
 */
public class DBSqlUtils {

    /**
     * 将使用逗号隔开的字段从oracle转换成sqlserver可以执行的字段
     *
     * @param fields
     * @return
     */
    public static String formatOracleToSqlServer(String fields) {
        if (StringUtils.isNotEmpty(fields)) {
            fields = "[" + fields + "]";
            fields = fields.replace(",", "],[");
            return fields;
        } else {
            return fields;
        }
    }

    /**
     * 得到可以执行的sql语句
     *
     * @param sql  sql语句
     * @param keys 关键字数组
     * @return
     */
    public static String getExecuteSql(String sql, String[] keys) {
        if (JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_ORACLE)) {
            return sql;
        } else {
            for (String key : keys) {
                sql = sql = sql.replace(key, "[" + key + "]");
            }
            return sql;
        }
    }

    public static Model getExtModel(String columnName, Integer columnType) {
        if (!Strings.isNullOrEmpty(columnName)) {
            String type = "auto";
//			Types
            if (columnType != null) {
                switch (columnType) {
                    case Types.VARCHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.CHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.CLOB:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.DECIMAL:
                        type = ExtFieldType.FLOAT;
                        break;
                    case Types.INTEGER:
                        type = ExtFieldType.INT;
                        break;
                    case Types.BLOB:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.VARBINARY:
                        type = ExtFieldType.INT;
                        break;
                    case Types.OTHER:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.SMALLINT:
                        type = ExtFieldType.INT;
                        break;
                    case Types.DOUBLE:
                        type = ExtFieldType.FLOAT;
                        break;
                    case Types.FLOAT:
                        type = ExtFieldType.FLOAT;
                        break;
                    case Types.LONGVARCHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.LONGNVARCHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.NUMERIC:
                        type = ExtFieldType.FLOAT;
                        break;
                    case Types.REAL:
                        type = ExtFieldType.FLOAT;
                        break;
                    case Types.NCHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.NVARCHAR:
                        type = ExtFieldType.STRING;
                        break;
                    case Types.DATE:
                        type = ExtFieldType.DATE;
                        break;
                    default:
                        type = ExtFieldType.AUTO;
                        break;
                }
            }
            return new Model(columnName, type);
        } else {
            return null;
        }
    }

    /**
     * 如果是oracle数据库，将值转换成string
     *
     * @param lists
     */
    public static void setClobList(List<Map> lists) {
        if (JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_ORACLE)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)) {
            for (Map map : lists) {
                for (Object key : map.keySet()) {
                    if (key != null) {
                        Object v = map.get(key);
                        if (v instanceof Clob) {
                            map.put(key, StringUtil.getClobValue(v));
                        }
                    }
                }
            }
        }
    }
}
