/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.db;

public class DbFieldType {
	public static String LIMIT="LIMIT";
	public static String TOTALCOUNT="TOTALCOUNT";
	public static String CODE="CODE";
	public static String NOWPAGE="NOWPAGE";
	public static String MSG="MSG";
	public static String CURSOR="CURSOR";
	public static String COLUMNCONFIG="COLUMNCONFIG";
	public static String ORDER="ORDER";
	public static String ORDERFIELD="ORDERFIELD";
	public static String ORDERTYPE="ORDERTYPE";
	public static String VARCHAR="VARCHAR";
	public static String INTEGER="INTEGER";
	public static String FLOAT="FLOAT";
	public static String SYSVAR="SYSVAR";
	public static String DOUBLE="DOUBLE";
	public static String BOOLEAN="BOOLEAN";
	public static String CLOB="CLOB";
	public static String DATE="DATE";
	public static String TIME="TIME";
	public static String NUMERIC="NUMERIC";
	public static String NCHAR="NCHAR";
	public static String VARBINARY="VARBINARY";
	public static String NCLOB="NCLOB";
	public static String DECIMAL="DECIMAL";
	public static String SMALLINT="SMALLINT";
	public static String TIMESTAMP="TIMESTAMP";
	public static String CHAR="CHAR";
	public static String LONGVARCHAR="LONGVARCHAR";
	public static String NVARCHAR="NVARCHAR";
	public static String BINARY="BINARY";
	public static String BIT="BIT";
	public static String BIGINT="BIGINT";
	public static String ROWID="ROWID";
	public static String STRUCT="STRUCT";
	public static String DISTINCT="DISTINCT";
	public static String LONGVARBINARY="LONGVARBINARY";
	public static String REAL="REAL";
	public static String JAVA_OBJECT="JAVA_OBJECT";
	public static String NULL="NULL";
	public static String TINYINT="TINYINT";
	public static String SQLXML="SQLXML";
	public static String DATALINK="DATALINK";
	public static String OTHER="OTHER";
	public static String LONGNVARCHAR="LONGNVARCHAR";
}
