/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service;

import com.je.common.base.DynaBean;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

/**
 * 抽象PlatformManager
 */
public interface PlatformService {

    default String getStringParameter(HttpServletRequest request, String key){
        if(StringUtil.isNotEmpty(request.getParameter(key))){
            return URLDecoder.decode(request.getParameter(key));
        }else {
            return request.getParameter(key);
        }
    }

    default Object getObjectParamter(HttpServletRequest request, String key) {
        if (request.getParameterMap().containsKey(key)) {
            return request.getParameter(key);
        } else {
            return request.getAttribute(key);
        }
    }

    ConditionsWrapper buildWrapper(BaseMethodArgument param, HttpServletRequest request);

    Page load(BaseMethodArgument param, HttpServletRequest request);

    DynaBean doSave(BaseMethodArgument param, HttpServletRequest request);

    DynaBean doCopy(BaseMethodArgument param, HttpServletRequest request);

    DynaBean doUpdate(BaseMethodArgument param, HttpServletRequest request);

    DynaBean doBatchUpdate(BaseMethodArgument param, HttpServletRequest request);

    int doRemove(BaseMethodArgument param, HttpServletRequest request);

    int doEnable(BaseMethodArgument param, HttpServletRequest request);

    int doDisable(BaseMethodArgument param, HttpServletRequest request);

    int doUpdateList(BaseMethodArgument param, HttpServletRequest request);

    List<DynaBean> doUpdateListAndReturn(BaseMethodArgument param, HttpServletRequest request);

    DynaBean getInfoById(BaseMethodArgument param, HttpServletRequest request);

    JSONTreeNode getTree(BaseMethodArgument param, HttpServletRequest request);

    List<Map<String, Object>> loadGridTree(BaseMethodArgument param, HttpServletRequest request);

    String checkFieldUnique(BaseMethodArgument param, HttpServletRequest request);

    void executeDataFlow(List<String> dataFlowIdList, HttpServletRequest request);

    DynaBean move(DynaBean dynaBean);

    List<Map<String,Object>> statistics(BaseMethodArgument param, HttpServletRequest request);

    int batchModifyListData(BaseMethodArgument param, HttpServletRequest request);

}
