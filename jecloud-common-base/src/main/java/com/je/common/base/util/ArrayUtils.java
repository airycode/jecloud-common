/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import net.sf.json.JSONArray;

import java.util.*;

/**
 * 数组工具类
 */
public class ArrayUtils {

	/**
	 * 通用的字符串分隔符
	 */
	public static final String SPLIT = ",";

	public static final String DOT = ".";

	/**
	 * 拼接两个字数串数组为一个新数组
	 * 如果两个参数数组均为null，则返回null
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static String[] concat(String[] array1, String[] array2) {
		if(null != array1 && null != array2) {
			int limit = array1.length + array2.length;
			String[] newArray = new String[limit];
			System.arraycopy(array1, 0, newArray, 0, array1.length);
			System.arraycopy(array2, 0, newArray, array1.length, array2.length);
			return newArray;
		} else if(null != array1 && null == array2) {
			return array1;
		} else if(null == array1 && null != array2) {
			return array2;
		} else {
			return null;
		}
	}

	/**
	 * 将一个形如：aaa,bbb,ccc,ddd的数组，转为形如'aaa','bbb','ccc','ddd'的字符串
	 * 如果ids为null或长度为0，则返回一个'NULL'
	 * @param ids
	 * @return
	 */
	public static String buildStringInSqlByArray(String[] ids) {
		StringBuffer idSql = new StringBuffer();
		if(null != ids && 0 != ids.length) {
			int i = 0;
			while(i<ids.length) {
				idSql.append("'");
				idSql.append(ids[i]);
				idSql.append("'");
				i++;
				if(i != ids.length) {
					idSql.append(ArrayUtils.SPLIT);
				}
			}
		} else {
			idSql.append("'NULL'");
		}
		return idSql.toString();
	}

	/**
	 * 查找数组是否包含
	 * @param array
	 * @param value
	 * @return
	 */
	public static Boolean contains(String[] array,String value){
		Boolean flag=false;
		if(Strings.isNullOrEmpty(value))return flag;
		for(String v:array){
			if(value.equalsIgnoreCase(v)){
				flag=true;
				break;
			}
		}
		return flag;
	}
	/**
	 * 查找value在数组的索引坐标
	 * @param array
	 * @param value
	 * @return
	 */
	public static Integer indexOf(String[] array,String value){
		Integer index=-1;
		for(Integer i =0;i<array.length;i++){
			if(value.equalsIgnoreCase(array[i])){
				index=i;
				break;
			}
		}
		return index;
	}
	/**
	 * 把集合转换成一个数组
	 * @param lists
	 * @return
	 */
	public static String[] getArray(Collection<String> lists){
		String[] arrays=new String[lists.size()];
		Integer i=0;
		for(String value:lists){
			arrays[i]=value;
			i++;
		}
		return arrays;
	}
	/**
	 * 把集合转换成一个数组
	 * @param lists
	 * @return
	 */
	public static String[] getArray4Json(JSONArray lists){
		String[] arrays=new String[lists.size()];
		Integer i=0;
		for(i=0;i<lists.size();i++){
			arrays[i]=lists.getString(i);
		}
		return arrays;
	}
	/**
	 * 把DynaBean集合中某字段值转换成数组
	 * @param lists
	 * @param fieldCode
	 * @return
	 */
	public static String[] getBeanFieldArray(Collection<DynaBean> lists, String fieldCode){
		String[] arrays=new String[lists.size()];
		Integer i=0;
		for(DynaBean bean:lists){
			arrays[i]=bean.getStr(fieldCode,"");
			i++;
		}
		return arrays;
	}
	/**
	 * 构建键值
	 * @param lists
	 * @param fieldCode
	 * @return
	 */
	public static Map<String,DynaBean> buildMapInfo(Collection<DynaBean> lists, String fieldCode){
		Map<String,DynaBean> infos=new HashMap<>();
		for(DynaBean dynaBean:lists){
			String key=dynaBean.getStr(fieldCode);
			if(StringUtil.isNotEmpty(key)){
				infos.put(key,dynaBean);
			}
		}
		return infos;
	}
	/**
	 * 构建键值集合
	 * @param lists
	 * @param fieldCode
	 * @return
	 */
	public static Map<String,List<DynaBean>> buildMapList(Collection<DynaBean> lists,String fieldCode){
		Map<String,List<DynaBean>> infos=new HashMap<>();
		for(DynaBean dynaBean:lists){
			String key=dynaBean.getStr(fieldCode);
			if(StringUtil.isNotEmpty(key)){
				List<DynaBean> datas=infos.get(key);
				if(datas==null){
					datas=new ArrayList<>();
				}
				datas.add(dynaBean);
				infos.put(key,datas);
			}
		}
		return infos;
	}
	/**
	 *
	 * 描述:array或list拼接
	 *
	 * @auther: wangmm@ketr.com.cn
	 * @date: 2019/3/6 15:26
	 */
	public static String join(String[] strs) {
		return join(strs, ArrayUtils.SPLIT);
	}
	public static String join(String[] strs,String split) {
		if(strs == null || strs.length == 0) return "";
		else return join(Arrays.asList(strs), split);
	}
	public static String join(List<String> strs) {
		return join(strs, ArrayUtils.SPLIT);
	}
	public static String join(List<String> strs, String split) {
		if(strs == null || strs.size() == 0) return "";
		StringBuffer idSql = new StringBuffer();
		for (String str : strs ) {
			idSql.append(str);
			idSql.append(split);
		}
		idSql.setLength(idSql.length()-1);
		return idSql.toString();
	}
}
