/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.datasource.runner;

import com.je.common.base.DynaBean;
import com.je.common.base.datasource.DynaResultSetCallable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**   
* Copyright: Copyright (c) 2018 jeplus.cn
* @Description: 动态Bean SQL执行器
* @version: v1.0.0
* @author: LIULJ
* @date: 2018年4月16日 下午9:26:37 
*
* Modification History:
* Date         Author          Version            Description
*---------------------------------------------------------*
* 2018年4月16日     LIULJ           v1.0.0               初始创建
*
*
*/
public class DynaSqlRunner extends SqlRunner {
	
	private static Map<String, DynaSqlRunner> dynaSqlRunnerMap = new HashMap<String, DynaSqlRunner>();
	
	public static DynaSqlRunner getInstance(String dataSourceName) {
		if(dynaSqlRunnerMap.get(dataSourceName) == null) {
			dynaSqlRunnerMap.put(dataSourceName, new DynaSqlRunner(dataSourceName));
		}
		return dynaSqlRunnerMap.get(dataSourceName);
	}
	
	/**
	 * 结果集回调器
	 */
	private DynaResultSetCallable rsc;
	
	/**
	 * @param dataSourceName
	 */
	public DynaSqlRunner(String dataSourceName) {
		super(dataSourceName);
		rsc = new DynaResultSetCallable();
	}
	
	/**
	 * 查询DynaBean
	 * @return
	 * @throws SQLException
	 */
	public List<DynaBean> queryDynaBeanList(String sql) throws SQLException{
		return queryBeanList(sql, rsc);
	}
	
}
