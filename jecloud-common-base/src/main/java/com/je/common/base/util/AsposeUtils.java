/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import cn.hutool.core.io.IoUtil;
import com.aspose.cells.PdfSaveOptions;
import com.aspose.cells.Workbook;
import com.aspose.slides.Presentation;
import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import com.google.common.io.Resources;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.xslf.usermodel.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.UUID;

public class AsposeUtils {
    public static Boolean initWordLinsense = false;
    public static Boolean initExcelLinsense = false;
    public static Boolean initPptLinsense = false;

    public static byte[] wordToPdf(byte[] content) {
        if (!initWordLinsense) {
            try {
                License asposeLicense = new License();
                URL url = Resources.getResource("license.xml");
                asposeLicense.setLicense(url.openStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            initWordLinsense = true;
        }
        ByteArrayOutputStream bos = null;
        InputStream inputStream = null;
        try {
            bos = new ByteArrayOutputStream();
            inputStream = new ByteArrayInputStream(content);
            Document document = new Document(inputStream);
            document.save(bos, SaveFormat.PDF);
            return bos.toByteArray();
        } catch (Exception e) {
//            log.error("字节数组转pdf字节数组失败！", e);
            return null;
        } finally {
            IoUtil.close(bos);
            IoUtil.close(inputStream);
        }
    }

    public static byte[] excelToPdf(byte[] content) {
        if (!initExcelLinsense) {
            try {
                com.aspose.cells.License asposeLicense = new com.aspose.cells.License();
                URL url = Resources.getResource("license.xml");
                asposeLicense.setLicense(url.openStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            initExcelLinsense = true;
        }
//        byte[] content= IoUtil.readBytes(wordIo);
        ByteArrayOutputStream bos = null;
        InputStream inputStream = null;
        try {
            bos = new ByteArrayOutputStream();
            inputStream = new ByteArrayInputStream(content);
            Workbook document = new Workbook(inputStream);
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
            pdfSaveOptions.setOnePagePerSheet(true);
            document.save(bos, pdfSaveOptions);
            return bos.toByteArray();
        } catch (Exception e) {
//            log.error("字节数组转pdf字节数组失败！", e);
            return null;
        } finally {
            IoUtil.close(bos);
            IoUtil.close(inputStream);
        }
    }

    public static byte[] pptToPdf(byte[] content) {
        if (!initPptLinsense) {
            try {
                com.aspose.slides.License asposeLicense = new com.aspose.slides.License();
                URL url = Resources.getResource("license.xml");
                asposeLicense.setLicense(url.openStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            initPptLinsense = true;
        }
//        byte[] content= IoUtil.readBytes(wordIo);
        ByteArrayOutputStream bos = null;
        InputStream inputStream = null;
        try {
            bos = new ByteArrayOutputStream();
            inputStream = new ByteArrayInputStream(content);
            Presentation document = new Presentation(inputStream);
            document.save(bos, com.aspose.slides.SaveFormat.Pdf);
            return bos.toByteArray();
        } catch (Exception e) {
//            log.error("字节数组转pdf字节数组失败！", e);
            return null;
        } finally {
            IoUtil.close(bos);
            IoUtil.close(inputStream);
        }
    }


    public static byte[] pptxToPdf(byte[] content) {
        if (!initPptLinsense) {
            try {
                com.aspose.slides.License asposeLicense = new com.aspose.slides.License();
                URL url = Resources.getResource("license.xml");
                asposeLicense.setLicense(url.openStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            initPptLinsense = true;
        }
//        byte[] content= IoUtil.readBytes(wordIo);
        ByteArrayOutputStream bos = null;
        InputStream inputStream = null;
        try {
            bos = new ByteArrayOutputStream();
            inputStream = new ByteArrayInputStream(content);
            Presentation document = new Presentation(inputStream);
            document.save(bos, com.aspose.slides.SaveFormat.Pdf);
            return bos.toByteArray();
        } catch (Exception e) {
//            log.error("字节数组转pdf字节数组失败！", e);
            return null;
        } finally {
            IoUtil.close(bos);
            IoUtil.close(inputStream);
        }
    }


}
