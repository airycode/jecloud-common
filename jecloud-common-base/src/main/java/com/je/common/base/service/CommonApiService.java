/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.api.VerificationParameterResultEntity;

import java.util.List;
import java.util.Map;

public interface CommonApiService {

    Map<String,Object> executeUpdate(DynaBean apiInfo, JSONArray parameterArr, List<DynaBean> inputParameter);

    Map<String,Object> executeInsert(DynaBean apiInfo, JSONArray parameterArr);

    List<Map<String,Object>> executeQuery(DynaBean apiInfo, JSONObject parameter, List<DynaBean> inputParameter, List<DynaBean> outputParameter);

    Map<String,Object> executeDelete(DynaBean apiInfo, JSONArray parameterArr, List<DynaBean> inputParameter);

    /**
     *
     * @param apiInfo ： 接口信息
     * @param parameterArr : 包含用户传参和输入模型的默认值
     * @return
     */
    VerificationParameterResultEntity checkParameter(DynaBean apiInfo,JSONArray parameterArr);

}
