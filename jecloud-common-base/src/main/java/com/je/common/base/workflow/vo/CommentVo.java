/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.workflow.vo;

public class CommentVo {
    /**
     * 审批人
     */
    private String userName;
    /**
     * 用户头像
     */
    private String userAvatar;
    /**
     * 审批人
     */
    private String userId;
    /**
     * 审批意见
     */
    private String comment = "";
    /**
     * 会签通过类型
     */
    private String commentType = "";
    /**
     * 操作类型
     */
    public OperationTypeEnum commentTypeEnum;
    /**
     * 审批时间
     */
    private String endTime;
    /**
     * 是否存在传阅信息
     */
    private boolean hasCirculationInformation = false;

    private String taskId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
        this.commentTypeEnum = OperationTypeEnum.getTypeByName(commentType);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public OperationTypeEnum getCommentTypeEnum() {
        return commentTypeEnum;
    }

    public void setCommentTypeEnum(OperationTypeEnum commentTypeEnum) {
        this.commentTypeEnum = commentTypeEnum;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public boolean isHasCirculationInformation() {
        return hasCirculationInformation;
    }

    public void setHasCirculationInformation(boolean hasCirculationInformation) {
        this.hasCirculationInformation = hasCirculationInformation;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
