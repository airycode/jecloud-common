/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log.factory.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.log.factory.SelectService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.HashMap;

public class SelectOneServiceImpl implements SelectService {


    private static final HashMap<String,String> map = new HashMap();

    static {
        map.put("tableCode@wrapper","selectOneByWrapper2p");
        map.put("tableCode@wrapper@columns","selectOneByWrapper3p");
    }

    @Override
    public String getSql(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodType = getMethodType(signature.getMethod(),map);
        JSONObject jsonObject = new JSONObject();
        switch (methodType){
            case "selectOneByWrapper2p":
                jsonObject = selectOneByWrapper2p(joinPoint.getArgs());
                break;

            case "selectOneByWrapper3p":
                jsonObject = selectOneByWrapper3p(joinPoint.getArgs());
                break;

        }
        return JSON.toJSONString(jsonObject);
    }

    private JSONObject selectOneByWrapper3p(Object[] args) {
        if(args==null||args.length!=3){
            return null;
        }

        if(args[1] instanceof ConditionsWrapper){
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[1];
            String tableCode = (String) args[0];
            if(StringUtil.isNotEmpty(tableCode)){
                conditionsWrapper.table(tableCode);
            }
            String columns = (String) args[2];
            if(StringUtil.isNotEmpty(columns)){
                conditionsWrapper.selectColumns(columns);
            }
            return   getSqlInfoByWrapper(conditionsWrapper);

        }
        return null;
    }

    private JSONObject selectOneByWrapper2p(Object[] args) {
        if(args==null||args.length!=2){
            return null;
        }

        if(args[1] instanceof ConditionsWrapper){
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[1];
            String tableCode = (String) args[0];
            if(StringUtil.isNotEmpty(tableCode)){
                conditionsWrapper.table(tableCode);
            }

          return   getSqlInfoByWrapper(conditionsWrapper);

        }

        return null;

    }
}
