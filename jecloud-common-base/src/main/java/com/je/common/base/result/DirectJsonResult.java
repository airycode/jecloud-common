/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.result;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import java.util.Map;

public class DirectJsonResult {

    /**
     * 构建返回JSONObject对象
     * @param obj
     * @return
     */
    public static JSONObject buildObjectResult(Object obj){
        if(obj == null){
            return null;
        }
        if(obj instanceof String){
            return JSON.parseObject(obj.toString());
        }
        if(obj instanceof JSONObject){
            return (JSONObject) obj;
        }
        return JSON.parseObject(JSON.toJSONString(obj));
    }

    /**
     * 构建返回JSONArray对象
     * @param obj
     * @return
     */
    public static JSONArray buildArrayResult(Object obj){
        if(obj == null){
            return null;
        }
        if(obj instanceof String){
            return JSON.parseArray(obj.toString());
        }
        if(obj instanceof JSONArray){
            return (JSONArray) obj;
        }
        return JSON.parseArray(JSON.toJSONString(obj));
    }

    public static JSONObject buildSuccessObjectResult(Object obj,boolean success){
        JSONObject result = buildObjectResult(obj);
        result.put("success",success);
        return result;
    }

    public static JSONObject buildAttributeResult(String key,Object value){
        JSONObject result = new JSONObject();
        result.put(key,value);
        return result;
    }

    public static JSONObject buildSuccessAttributeResult(String key,Object value,boolean success){
        JSONObject result = new JSONObject();
        result.put(key,value);
        result.put("success",success);
        return result;
    }

    public static JSONObject buildAttributeResultWithMap(Map<String,Object> map){
        JSONObject result = new JSONObject();
        result.putAll(map);
        return result;
    }

    public static JSONObject buildSuccessAttributeResultWithMap(Map<String,Object> map){
        JSONObject result = new JSONObject();
        result.putAll(map);
        return result;
    }
}
