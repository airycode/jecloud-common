/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.portal.vo;

/**
 * 冒泡vo
 */
public class Bubble {

    private String type;

    private String category;

    private String position;

    private int start;

    private int limit;

    private String MPXX_XXNR;

    private String MPXX_MPTP;

    private String MPXX_MPXX_STATE;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getMPXX_XXNR() {
        return MPXX_XXNR;
    }

    public void setMPXX_XXNR(String MPXX_XXNR) {
        this.MPXX_XXNR = MPXX_XXNR;
    }

    public String getMPXX_MPTP() {
        return MPXX_MPTP;
    }

    public void setMPXX_MPTP(String MPXX_MPTP) {
        this.MPXX_MPTP = MPXX_MPTP;
    }

    public String getMPXX_MPXX_STATE() {
        return MPXX_MPXX_STATE;
    }

    public void setMPXX_MPXX_STATE(String MPXX_MPXX_STATE) {
        this.MPXX_MPXX_STATE = MPXX_MPXX_STATE;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
