/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

/**
 * 通用缓存定义，用于封装通用缓存，比如ehcache或guava cache
 * @param <V> 值
 */
public interface Cache<V> {

    /**
     * 超时同步的Key,此缓存中包含要同步的Key
     */
    String SYNC_KEY_PREFIX = "cacheSyncTempCache";

    /**
     * 同步超时时间，默认是60s
     */
    long SYNC_TIMEOUT = 60;

    String SPLITTER = "/";

    /**
     * 没有租户的默认的KEY后缀
     */
    String NO_TENANT_KEY = "global";

    String getCacheKey();

    /**
     * 获取名称
     * @return
     */
    String getName();

    default String getDesc(){
        return null;
    }

    /**
     * 获取租户下的键值
     * @param zhId
     * @return
     */
    default String getTenantCacheKey(String zhId){
        return String.format("%s%s%s",getCacheKey(),SPLITTER,zhId);
    }

    /**
     * 清空
     */
    void clear();

    /**
     * 清空
     */
    void asyncClear();

    /**
     * 清楚本地缓存
     */
    void localClear();

    /**
     * 清楚本地租户缓存
     */
    void localClear(String tenantId);

    /**
     * 清空租户缓存
     * @param tenantId
     */
    void clear(String tenantId);

    /**
     * 获取当前cache的beanName
     * @return
     */
    default String getBeanName(){
        return getCacheKey();
    }

    /**
     * 获取所属服务
     * @return
     */
    String getService();

    /**
     * 是否启用一级缓存，默认为启用一级缓存
     * @return
     */
    default boolean enableFristLevel(){
        return true;
    }

    /**
     * 注册缓存，用于缓存管理
     */
    void regist();

}
