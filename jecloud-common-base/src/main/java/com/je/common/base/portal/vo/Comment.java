/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.portal.vo;

/**
 * 评论vo
 */
public class Comment {

    private String id;

    private String userId;

    private String userName;

    private String type;

    private int start;

    private int limit;

    private String COMMENT_LJ;

    private String COMMENT_LX;

    private String COMMENT_YW_ID;

    private String COMMENT_YW_CODE;

    private String COMMENT_YW_NAME;

    private String COMMENT_PLNR;

    private String COMMENT_FJPL_ID;

    private String COMMENT_PLTP;

    private String MPXX_XXNR;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getCOMMENT_LJ() {
        return COMMENT_LJ;
    }

    public void setCOMMENT_LJ(String COMMENT_LJ) {
        this.COMMENT_LJ = COMMENT_LJ;
    }

    public String getCOMMENT_LX() {
        return COMMENT_LX;
    }

    public void setCOMMENT_LX(String COMMENT_LX) {
        this.COMMENT_LX = COMMENT_LX;
    }

    public String getCOMMENT_YW_ID() {
        return COMMENT_YW_ID;
    }

    public void setCOMMENT_YW_ID(String COMMENT_YW_ID) {
        this.COMMENT_YW_ID = COMMENT_YW_ID;
    }

    public String getCOMMENT_YW_CODE() {
        return COMMENT_YW_CODE;
    }

    public void setCOMMENT_YW_CODE(String COMMENT_YW_CODE) {
        this.COMMENT_YW_CODE = COMMENT_YW_CODE;
    }

    public String getCOMMENT_YW_NAME() {
        return COMMENT_YW_NAME;
    }

    public void setCOMMENT_YW_NAME(String COMMENT_YW_NAME) {
        this.COMMENT_YW_NAME = COMMENT_YW_NAME;
    }

    public String getCOMMENT_PLNR() {
        return COMMENT_PLNR;
    }

    public void setCOMMENT_PLNR(String COMMENT_PLNR) {
        this.COMMENT_PLNR = COMMENT_PLNR;
    }

    public String getCOMMENT_FJPL_ID() {
        return COMMENT_FJPL_ID;
    }

    public void setCOMMENT_FJPL_ID(String COMMENT_FJPL_ID) {
        this.COMMENT_FJPL_ID = COMMENT_FJPL_ID;
    }

    public String getCOMMENT_PLTP() {
        return COMMENT_PLTP;
    }

    public void setCOMMENT_PLTP(String COMMENT_PLTP) {
        this.COMMENT_PLTP = COMMENT_PLTP;
    }

    public String getMPXX_XXNR() {
        return MPXX_XXNR;
    }

    public void setMPXX_XXNR(String MPXX_XXNR) {
        this.MPXX_XXNR = MPXX_XXNR;
    }
}
