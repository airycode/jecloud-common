/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.exception;

import com.je.common.base.locale.MessageLocator;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class APIWarnException extends Exception{

    private String code;
    private String errorCode;
    private String errorMsg;
    private Object[] errorParam;

    public APIWarnException() {
        super();
    }

    public APIWarnException(Throwable cause) {
        super(cause);
    }

    public APIWarnException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
        this.errorMsg = MessageLocator.getMessage(this.errorCode, message);
    }

    public APIWarnException(String message, String errorCode, Throwable cause) {
        super(message, cause);
        this.code="9999";
        this.errorCode = errorCode;
        this.errorMsg = MessageLocator.getMessage(this.errorCode, message);
    }
    public APIWarnException(String message, String errorCode, Object[] params) {
        super(message);
        this.code="9999";
        this.errorCode = errorCode;
        this.errorMsg = MessageLocator.getMessage(this.errorCode,params, message);
    }

    public APIWarnException(String message, String errorCode, Object[] params, Throwable cause) {
        super(message, cause);
        this.code="9999";
        this.errorCode = errorCode;
        this.errorMsg = MessageLocator.getMessage(this.errorCode, params, message);
        this.errorParam = params;
    }
    public APIWarnException(String message, String errorCode, HttpServletRequest request) {
        this(message,errorCode,request,null);
    }
    public APIWarnException(String message, String errorCode, HttpServletRequest request, Throwable cause) {
        super(message, cause);
        Map<String,Object> params=new HashMap<>();
        Enumeration<String> enumerations=request.getParameterNames();
        while(enumerations.hasMoreElements()){
            String key=enumerations.nextElement();
            params.put(key,request.getParameter(key));
        }
        String uri = request.getRequestURI();
        String tokenId="";

        this.code="9999";
        this.errorCode = errorCode;
        this.errorMsg = MessageLocator.getMessage(this.errorCode, new Object[]{uri,tokenId,params}, message);
        this.errorParam = new Object[]{uri,tokenId,params};
    }

    @Override
    public String getLocalizedMessage() {
        return errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public Object[] getErrorParam() {
        return errorParam;
    }

    public String getCode() {
        return code;
    }
}
